   -- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  mar. 27 oct. 2020 à 08:57
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `inetschool_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201023033131', '2020-10-23 04:31:58', 525),
('DoctrineMigrations\\Version20201023230552', '2020-10-24 00:06:09', 259),
('DoctrineMigrations\\Version20201024211710', '2020-10-24 22:17:23', 146),
('DoctrineMigrations\\Version20201026155908', '2020-10-26 16:59:27', 383);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `nom`, `prenom`, `type`) VALUES
(45, 'Chakira@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$xa+grwodIN9Xm1R40tGXgw$aMYHDeBGkPUNj378XQOeQUbq4hhPiXMEFi3os7ksJs0', 'FOFANA', 'Chakira', 6),
(49, 'francisagbannon@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$VAIoIsQu+HuzLIlL8+1FAw$1Rfrg1Rh9/HF2O4RSPrFyqrIcFQVEZy3+42zMqH7z7E', 'AGBANNON', 'Francis', 1),
(62, 'ainamongannan@gmail.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$Wacc/tx2t8aRkCBkqq9JaQ$yQPCrbSS8KT2PAIK3GUw6N+zWTuS0eku8KhkGZqX90g', 'gannan', 'emile', 6),
(72, 'painamon@outlook.com', '[]', '$argon2id$v=19$m=65536,t=4,p=1$tacM5MawEfLk+e7/aMeECw$ooOZHYvncSWoVTEsMimxNqx6h9Gi7cDMdtqhQaGGSRI', 'Ainamon', 'Patrick', 6);

-- --------------------------------------------------------

--
-- Structure de la table `user_joined_enseigne`
--

CREATE TABLE `user_joined_enseigne` (
  `id` int(11) NOT NULL,
  `id_enseigne` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `profiles` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  `droits` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_joined_enseigne`
--

INSERT INTO `user_joined_enseigne` (`id`, `id_enseigne`, `id_user`, `profiles`, `droits`) VALUES
(1, 3, 49, 'a:2:{i:0;s:6:\"parent\";i:1;s:10:\"enseignant\";}', 'N;'),
(2, 3, 72, 'a:2:{i:0;s:11:\"surveillant\";i:1;s:7:\"censeur\";}', 'a:1:{i:0;s:17:\"co_administrateur\";}'),
(4, 8, 62, 'a:0:{}', 'a:0:{}'),
(5, 3, 45, 'a:0:{}', 'a:1:{i:0;s:17:\"co_administrateur\";}'),
(6, 4, 49, 'a:2:{i:0;s:10:\"secretaire\";i:1;s:16:\"directeur_etudes\";}', 'a:0:{}'),
(7, 8, 49, 'a:0:{}', 'a:0:{}');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- Index pour la table `user_joined_enseigne`
--
ALTER TABLE `user_joined_enseigne`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `user_joined_enseigne`
--
ALTER TABLE `user_joined_enseigne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
