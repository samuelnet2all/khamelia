<?php

namespace App\Repository;

use App\Entity\SpecialiteEnseigne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpecialiteEnseigne|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecialiteEnseigne|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecialiteEnseigne[]    findAll()
 * @method SpecialiteEnseigne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialiteEnseigneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecialiteEnseigne::class);
    }

    // /**
    //  * @return SpecialiteEnseigne[] Returns an array of SpecialiteEnseigne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpecialiteEnseigne
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
