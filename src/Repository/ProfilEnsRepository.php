<?php

namespace App\Repository;

use App\Entity\ProfilEns;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProfilEns|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfilEns|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfilEns[]    findAll()
 * @method ProfilEns[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfilEnsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfilEns::class);
    }

    // /**
    //  * @return ProfilEns[] Returns an array of ProfilEns objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProfilEns
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
