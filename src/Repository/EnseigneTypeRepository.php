<?php

namespace App\Repository;

use App\Entity\EnseigneType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EnseigneType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EnseigneType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EnseigneType[]    findAll()
 * @method EnseigneType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EnseigneTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EnseigneType::class);
    }

    // /**
    //  * @return EnseigneType[] Returns an array of EnseigneType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EnseigneType
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
