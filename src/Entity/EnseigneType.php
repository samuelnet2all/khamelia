<?php

namespace App\Entity;

use App\Repository\EnseigneTypeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EnseigneTypeRepository::class)
 */
class EnseigneType
{
   
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\OneToMany(targetEntity="App\Entity\TypeEns")
     * @ORM\ManyToMany(targetEntity="App\Entity\SpecialiteEnseigne")
     */
    private $id_enseigne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    public function getIdEnseigne(): ?int
    {
        return $this->id_enseigne;
    }

    public function setIdEnseigne(int $id_enseigne): self
    {
        $this->id_enseigne = $id_enseigne;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
