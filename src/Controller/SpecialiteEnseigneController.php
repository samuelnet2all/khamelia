<?php

namespace App\Controller;

use App\Entity\SpecialiteEnseigne;
use App\Form\SpecialiteEnseigneType;
use App\Repository\SpecialiteEnseigneRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/specialite/enseigne")
 */
class SpecialiteEnseigneController extends AbstractController
{
    /**
     * @Route("/", name="specialite_enseigne_index", methods={"GET"})
     */
    public function index(SpecialiteEnseigneRepository $specialiteEnseigneRepository): Response
    {
        return $this->render('specialite_enseigne/index.html.twig', [
            'specialite_enseignes' => $specialiteEnseigneRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="specialite_enseigne_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $specialiteEnseigne = new SpecialiteEnseigne();
        $form = $this->createForm(SpecialiteEnseigneType::class, $specialiteEnseigne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($specialiteEnseigne);
            $entityManager->flush();

            return $this->redirectToRoute('specialite_enseigne_index');
        }

        return $this->render('specialite_enseigne/new.html.twig', [
            'specialite_enseigne' => $specialiteEnseigne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="specialite_enseigne_show", methods={"GET","POST"})
     */
    public function show(SpecialiteEnseigne $specialiteEnseigne): Response
    {
        return $this->render('specialite_enseigne/show.html.twig', [
            'specialite_enseigne' => $specialiteEnseigne,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="specialite_enseigne_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SpecialiteEnseigne $specialiteEnseigne): Response
    {
        $form = $this->createForm(SpecialiteEnseigneType::class, $specialiteEnseigne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('specialite_enseigne_index');
        }

        return $this->render('specialite_enseigne/edit.html.twig', [
            'specialite_enseigne' => $specialiteEnseigne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="specialite_enseigne_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SpecialiteEnseigne $specialiteEnseigne): Response
    {
        if ($this->isCsrfTokenValid('delete'.$specialiteEnseigne->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($specialiteEnseigne);
            $entityManager->flush();
        }

        return $this->redirectToRoute('specialite_enseigne_index');
    }
}
