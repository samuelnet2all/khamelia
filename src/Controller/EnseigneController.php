<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use MercurySeries\FlashyBundle\FlashyNotifier;
use App\Repository\UserJoinedEnseigneRepository;
use App\Entity\UserJoinedEnseigne;
use App\Repository\UserRepository;
use App\Repository\EnseigneTypeRepository;
use App\Entity\EnseigneType;

/**
 * @Route("/dashboard/enseigne")
 */ 
class EnseigneController extends AbstractController
{
	private $apiMyEnseigneUrl = 'my_enseignes.php';
	private $apiAllEnseigneAffilierUrl = 'all_enseigne_affilier.php';
	private $apiAffilierEnseigneCommunaute = 'affilier_enseigne_communaute.php';
	private $apiUpdateEnseigne = 'update_enseigne.php';
    private $apiAddEnseigne = 'add_enseigne.php';

    /**
     * @Route("/create", name="create_enseigne", methods={"POST"})
     */
    public function create(Request $request, HttpClientInterface $client, FlashyNotifier $flashy) 
    {
        $user = $this->getUser();
        if (!$user)
        {
            return $this->redirectToRoute('app_login');
        }

        $submittedToken = $request->request->get('_token');
        if ($this->isCsrfTokenValid('create-item'.$user->getId(), $submittedToken)) 
        {
            $nom_enseigne = $request->request->get('nom_enseigne');
            $code_enseigne = $request->request->get('code_enseigne');
            $id_entreprise = $request->request->get('id_entreprise');

            $response = $client->request('POST', $this->getParameter('API_URL').$this->apiAddEnseigne, [
                    'body' => [
                        'nom_enseigne' => $nom_enseigne,
                        'code_enseigne' => $code_enseigne,
                        'id_entreprise' => $user->getId()
                    ]
                ]);

            $content = $response->getContent();
            $content_array = json_decode($content, true);
            $isSuccess = key_exists("server_response", $content_array) ? $content_array['server_response'][0]['status'] : 0;

            if ($isSuccess == 1) {
                    $flashy->success("Création réussie");
                return $this->redirectToRoute('enseigne');
            }
            else
            {
                $flashy->error("Echec de la création");
                return $this->redirectToRoute('enseigne');
            }
        }
        else
        {
            $flashy->error("Formulaire invalide");
            return $this->redirectToRoute('enseigne');
        }

    }

    /**
     * @Route("/", name="enseigne", methods={"GET"})
     */
    public function index(Request $request, HttpClientInterface $client, UserJoinedEnseigneRepository $userJoinedEnseigneRepository, EnseigneTypeRepository $enseignetypeRepo): Response
    {
    	$user = $this->getUser();
    	if (!$user)
    	{
    		return $this->redirectToRoute('app_login');
    	}

        $enseigneType = $enseignetypeRepo->findAll();
    	
    	//Liste des enseignes affilié à InetSCHOOLS
		$responseEnseigneAffilier = $client->request('POST', $this->getParameter('API_URL').$this->apiAllEnseigneAffilierUrl, [
    			'body' => [
    				'id_communaute' => $this->getParameter('ID_COMMUNAUTE')
    			]
    		]);

    		$content = $responseEnseigneAffilier->getContent();
    		$content_array = json_decode($content, true);
    		$AllEnseigneAffiliees= $content_array['server_responses'][0]['founded'] === 0 ? [] : $content_array['server_responses'];


    	//Obtenir la liste des enseignes rejoints par l'utilisateur connecté
		$enseignesJoined = $userJoinedEnseigneRepository->findBy(['id_user' => $user->getId()]);
		$joined = [];
		foreach ($enseignesJoined as $enseigneJoined)
		{
			foreach ($AllEnseigneAffiliees as $enseigneAffiliee) 
			{
				if($enseigneJoined->getIdEnseigne() == $enseigneAffiliee['id_enseigne'])
					array_push($joined, $enseigneJoined->getIdEnseigne());
			}
		}

        $enseigneTypes = $enseignetypeRepo->findAll();


    	if ($user->getType() === $this->getParameter('TYPE_PARTICULIER')) 
    	{
    		
    		return $this->render('enseigne/index.html.twig', [
	            'affiliées' => $AllEnseigneAffiliees,
	            'joined' => $joined,
	            'enseignesJoined' => $enseignesJoined,
	            'position' => 'Enseigne',
	            'chemin' => 'Enseigne',
                'enseigneTypes' => $enseigneTypes
	        ]);
    	}
    	elseif ($user->getType() === $this->getParameter('TYPE_ENTREPRISE')) 
    	{
    		//Liste des enseignes créer par l'entreprise connectée
	    	$responseMyEnseigne = $client->request('POST', $this->getParameter('API_URL').$this->apiMyEnseigneUrl, [
	    			'body' => [
	    				'id_entreprise' => $user->getId()
	    			]
				]);

			$content = $responseMyEnseigne->getContent();
			$content_array = json_decode($content, true);
			$MyEnseignes = $content_array['server_responses'][0]['founded'] === 0 ? [] : $content_array['server_responses'];
    		
    		
    		$affiliées = [];
    		$myEnseignesId = [];
            $enseignesTypes = [];
    		if(!empty($MyEnseignes) and !empty($AllEnseigneAffiliees) )
    		{
	    		foreach ($MyEnseignes as $enseigne) {
	    			array_push($myEnseignesId, $enseigne['id_enseigne']);
	    			foreach ($AllEnseigneAffiliees as $enseigneAffilie) {
	    				if($enseigne['id_enseigne'] == $enseigneAffilie['id_enseigne'])
	    					array_push($affiliées, $enseigne['id_enseigne']) ;
	    			}
                    foreach ($enseigneTypes as $value) {
                        if($value->getIdEnseigne() == $enseigne['id_enseigne'])       
                            $enseignesTypes[$enseigne['id_enseigne']] = $value->getType();        
                    }
	    		}
    		}
    		

    		return $this->render('enseigne/index.html.twig', [
	            'enseignes' => $MyEnseignes,
	            'affiliées' => $affiliées,
	            'enseignesAffiliees' => $AllEnseigneAffiliees,
	            'myEnseignesId' => $myEnseignesId,
	            'enseignesJoined' => $enseignesJoined,
	            'joined' => $joined,
	            'position' => 'Enseigne',
	            'chemin' => 'Enseigne',
                'enseignesTypes' => $enseignesTypes
	        ]);
    	}
        
    }

    /**
     * @Route("/affilier/{id_enseigne}", name="affilier", methods={"GET"})
     */
    public function affilier(Request $request, $id_enseigne, HttpClientInterface $client, FlashyNotifier $flashy)
    {
    	$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}

    	$response = $client->request('POST', $this->getParameter('API_URL').$this->apiAffilierEnseigneCommunaute, [
    		'body' => [
    			'id_communaute' => $this->getParameter('ID_COMMUNAUTE'),
    			'id_enseigne' => $id_enseigne
    		]
    	]);

    	$content = $response->getContent();
    	$content_array = json_decode($content, true);
    	$isSuccess = $content_array['server_responses'];
    	if($isSuccess == 1)
    	{
    		$message = "Affiliation réussie";
    		
    	}
    	elseif ($isSuccess == 0) 
    	{
    		$message = "Echec de l'Affiliation";
    	}
    	elseif ($isSuccess == -1) 
    	{
    		$message = "Affiliation déjà existante!";
    		
    	}

    	$flashy->success($message);
    	return $this->redirectToRoute('enseigne');
    }

    /**
     * @Route("/rejoindre/{id_enseigne}", name="rejoindre", methods={"GET"})
     */
    public function rejoindre(Request $request, HttpClientInterface $client, FlashyNotifier $flashy, UserJoinedEnseigneRepository $userJoinedEnseigneRepository, $id_enseigne)
    {
    	$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}
    	$userJoinedEnseigne = new UserJoinedEnseigne();
    	$userJoinedEnseigne->setIdEnseigne($id_enseigne)
    					   ->setIdUser($user->getId());

    	$entityManager = $this->getDoctrine()->getManager();
    	$entityManager->persist($userJoinedEnseigne);
    	$entityManager->flush();

    	$message = "Succès de l'intégration";
    	$flashy->success($message);
    	return $this->redirectToRoute('enseigne');

    }  

    /**
     * @Route("/membres/{id_enseigne}", name="membre", methods={"GET", "POST"})
     */
    public function membres(Request $request, FlashyNotifier $flashy, $id_enseigne, UserJoinedEnseigneRepository $userJoinedEnseigneRepository, UserRepository $userRepository) 
    {
    	$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}

    	$usersJoined= $userJoinedEnseigneRepository->findBy(['id_enseigne' => $id_enseigne]);
    	$users = $userRepository->findAll();
    	$usersArray = [];

    	foreach ($usersJoined as $userJoined) {
    		foreach ($users as $user) {
    			if($user->getId() == $userJoined->getIdUser())
    			{
    				$usersArray[$user->getId()] = $user;
    			}
    		}
    	}

    	return $this->render('enseigne/membres.html.twig', [
	            'usersArray' => $usersArray,
	            'usersJoined' => $usersJoined,
	            'position' => 'Liste des membres',
	            'chemin' => 'Enseigne  /  Liste des membres',
	        ]);
	}

	/**
	 * @Route("/profile", name="profile", methods={"POST"})
	 */
	public function profiles(Request $request, FlashyNotifier $flashy, UserJoinedEnseigneRepository $userJoinedEnseigneRepository, UserRepository $userRepository) 
	{
		$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}

    	$submittedToken = $request->request->get('_token');
    	if ($this->isCsrfTokenValid('profiles-item'.$user->getId(), $submittedToken)) 
    	{
    		$id_enseigne = $request->request->get('id_enseigne');
    		$id_user = $request->request->get('id_user');	
    		$profiles = $request->request->get('profiles');


    		$userJoined= $userJoinedEnseigneRepository->findOneBy(['id_enseigne' => $id_enseigne, 'id_user' => $id_user]);
    		$userJoined->setProfiles($profiles);
    		$entityManager = $this->getDoctrine()->getManager();
	    	$entityManager->flush();

	    	$flashy->success("Profiles assigné(s) avec succès");
	    	return $this->redirectToRoute('membre', ['id_enseigne' => $id_enseigne]);
    	}
    	else
    	{
    		$flashy->error("Formulaire invalide");
	    	return $this->redirectToRoute('membre', ['id_enseigne' => $id_enseigne]);
    	}
	}

	/**
	 * @Route("/droits", name="droits", methods={"POST"})
	 */
	public function roles(Request $request, FlashyNotifier $flashy, UserJoinedEnseigneRepository $userJoinedEnseigneRepository, UserRepository $userRepository) 
	{
		$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}

    	$submittedToken = $request->request->get('_token');
    	if ($this->isCsrfTokenValid('droits-item'.$user->getId(), $submittedToken)) 
    	{
    		$id_enseigne = $request->request->get('id_enseigne');
    		$id_user = $request->request->get('id_user');	
    		$droits = $request->request->get('droits');


    		$userJoined= $userJoinedEnseigneRepository->findOneBy(['id_enseigne' => $id_enseigne, 'id_user' => $id_user]);
    		$userJoined->setDroits($droits);
    		$entityManager = $this->getDoctrine()->getManager();
	    	$entityManager->flush();

	    	$flashy->success("Droits assignés avec succès");
	    	return $this->redirectToRoute('membre', ['id_enseigne' => $id_enseigne]);
    	}
    	else
    	{
    		$flashy->error("Formulaire invalide");
	    	return $this->redirectToRoute('membre', ['id_enseigne' => $id_enseigne]);
    	}
	}

	/**
	 * @Route("/edit/", name="edit_enseigne", methods={"POST"})
	 */

	public function edit(Request $request, FlashyNotifier $flashy, HttpClientInterface $client, EnseigneTypeRepository $enseignetypeRepo)
	{
		$user = $this->getUser();
    	if (!$user) {
    		return $this->redirectToRoute('app_login');
    	}
    	$submittedToken = $request->request->get('_token');
    	

    	$id_enseigne = $request->request->get('id_enseigne');
    	if ($this->isCsrfTokenValid('edit-item'.$user->getId(), $submittedToken)) 
    	{
	    	$response = $client->request('POST', $this->getParameter('API_URL').$this->apiUpdateEnseigne, [
	    		'body' => [
	    			'nom_enseigne' => $request->request->get('nom_enseigne'),
	    			'code_enseigne' => $request->request->get('code_enseigne'),
	    			'id_enseigne' => $id_enseigne
	    		]
	    	]);

	    	$content = $response->getContent();
	    	$content_array = json_decode($content, true);

	    	$isSuccess = key_exists("server_response", $content_array) ? $content_array['server_response'][0]['status'] : 0;
             $entityManager = $this->getDoctrine()->getManager();
	    	if ($isSuccess == 1) {
                $enseignetypeResu = $enseignetypeRepo->findOneBy(['id_enseigne' => $id_enseigne]);
                if(!$enseignetypeResu)
                {
                    $enseigneType = new EnseigneType();
                    $enseigneType->setIdEnseigne($id_enseigne)
                                 ->setType($request->request->get('type')); 


                   
                    $entityManager->persist($enseigneType);
                    $entityManager->flush();
                }
                else
                {
                    $enseignetypeResu->setType($request->request->get('type'));

                    $entityManager->flush();
                }
                

	    		$flashy->success("Mise à jour réussie");
	    		return $this->redirectToRoute('enseigne');
	    	}
	    	else
	    	{
	    		$flashy->error("Echec de la mise à jour");
	    		return $this->redirectToRoute('enseigne');
	    	}
	    }
	   	else
    	{
    		$flashy->error("Formulaire invalide");
	    	return $this->redirectToRoute('membre', ['id_enseigne' => $id_enseigne]);
    	}

	} 
}
