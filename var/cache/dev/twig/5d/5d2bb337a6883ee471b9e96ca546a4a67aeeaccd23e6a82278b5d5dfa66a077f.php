<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* dashboard/index.html.twig */
class __TwigTemplate_89e3f6934c03d3e8c6847afd0c8cc6c6afcac569dbc323a82e0690eb2207ba78 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "dashboard/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "dashboard/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "dashboard/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "KhaMELIA - Dashboard";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<!-- start: page -->
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8\">
\t\t\t\t\t\t\t\t\t\t<div class=\"chart-data-selector\" id=\"salesSelectorWrapper\">
\t\t\t\t\t\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t\t\t\t\t\tSales:
\t\t\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"salesSelector\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Admin\" selected>JSOFT Admin</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Drupal\" >JSOFT Drupal</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Wordpress\" >JSOFT Wordpress</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t\t</h2>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"salesSelectorItems\" class=\"chart-data-selector-items mt-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Admin -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Admin\" id=\"flotDashSales1\" class=\"chart-active\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales1Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 190],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 320],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 270],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 180]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#0088cc\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Drupal -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Drupal\" id=\"flotDashSales2\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales2Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 290],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 540],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 480],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 220],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 170],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 190]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#2baab1\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Wordpress -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Wordpress\" id=\"flotDashSales3\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales3Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 840],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 740],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 690],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 940],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 1180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 820],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 570],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 780]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#734ba9\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 text-center\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title mt-md\">Sales Goal</h2>
\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-wrapper liquid-meter-sm mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter\">
\t\t\t\t\t\t\t\t\t\t\t\t<meter min=\"0\" max=\"100\" value=\"35\" id=\"meterSales\"></meter>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-selector\" id=\"meterSalesSel\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"35\" class=\"active\">Monthly Goal</a>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"28\">Annual Goal</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-primary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-life-ring\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Support Questions</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">1281</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-primary\">(14 unread)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(view all)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-secondary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-secondary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-usd\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Total Profit</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">\$ 14,890.30</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(withdraw)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-tertiary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-tertiary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Orders</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">38</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(statement)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-quartenary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-quartenary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Visitors</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">3765</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(report)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Profile</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-group\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading bg-primary\">

\t\t\t\t\t\t\t\t\t\t<div class=\"widget-profile-info\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name text-semibold\">John Doe</h4>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"role\">Administrator</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">(edit profile)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div id=\"accordion\">
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion panel-accordion-first\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1One\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check\"></i> Tasks
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1One\" class=\"accordion-body collapse in\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"widget-todo-list\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"\" id=\"todoListItem1\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem1\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem2\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem2\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem3\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem3\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem4\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem4\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem5\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem5\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem6\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem6\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr class=\"solid mt-sm mb-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<form method=\"get\" class=\"form-horizontal form-bordered\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" tabindex=\"-1\">Add</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1Two\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"fa fa-comment\"></i> Messages
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1Two\" class=\"accordion-body collapse\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list mb-xlg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineBarDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">488</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Average Profile Visits</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar circular-bar-xs m-none mt-xs mr-md pull-right\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar-chart\" data-percent=\"45\" data-plugin-options='{ \"barColor\": \"#2baab1\", \"delay\": 300, \"size\": 50, \"lineWidth\": 4 }'>
\t\t\t\t\t\t\t\t\t\t\t\t<strong>Average</strong>
\t\t\t\t\t\t\t\t\t\t\t\t<label><span class=\"percent\">45</span>%</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">12</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Working Projects</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineLineDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">89</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Pending Tasks</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t<span class=\"label label-primary label-sm text-normal va-middle mr-sm\">198</span>
\t\t\t\t\t\t\t\t\t<span class=\"va-middle\">Friends</span>
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<hr class=\"dotted short\">
\t\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t<a class=\"text-uppercase text-muted\" href=\"#\">(View All)</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"panel-footer\">
\t\t\t\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-6 col-lg-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Company Activity</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"timeline timeline-simple mt-xlg mb-md\">
\t\t\t\t\t\t\t\t\t<div class=\"tm-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"tm-title\">
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"h5 text-uppercase\">November 2013</h3>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<ol class=\"tm-items\">
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tIt's awesome when we find a good solution for our projects, JSOFT Admin is <span class=\"text-primary\">#awesome</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tCheckout! How cool is that!
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"thumbnail-gallery\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"img-thumbnail lightbox\" href=\"assets/images/projects/project-4.jpg\" data-plugin-options='{ \"type\":\"image\" }'>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" width=\"215\" src=\"assets/images/projects/project-4.jpg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"zoom\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Global Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div id=\"vectorMapWorld\" style=\"height: 350px; width: 100%;\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Projects Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-striped mb-none\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Project</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Progress</th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>1</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>2</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>3</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t60%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>4</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive Business WordPress Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t90%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>5</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Admin HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 45%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t45%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>6</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-danger\">Danger</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t40%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>7</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 95%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t95%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- end: page -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 667
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 668
        echo "\t<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 669
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 670
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-appear/jquery.appear.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 671
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 672
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-easypiechart/jquery.easypiechart.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 673
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 674
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot-tooltip/jquery.flot.tooltip.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 675
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 676
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.categories.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 677
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 678
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-sparkline/jquery.sparkline.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 679
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/raphael/raphael.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 680
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/morris/morris.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 681
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/gauge/gauge.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 682
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/snap-svg/snap.svg.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 683
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/liquid-meter/liquid.meter.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 684
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/jquery.vmap.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 685
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 686
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/jquery.vmap.world.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 687
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 688
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 689
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 690
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 691
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 692
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "dashboard/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  866 => 692,  862 => 691,  858 => 690,  854 => 689,  850 => 688,  846 => 687,  842 => 686,  838 => 685,  834 => 684,  830 => 683,  826 => 682,  822 => 681,  818 => 680,  814 => 679,  810 => 678,  806 => 677,  802 => 676,  798 => 675,  794 => 674,  790 => 673,  786 => 672,  782 => 671,  778 => 670,  774 => 669,  769 => 668,  759 => 667,  89 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}KhaMELIA - Dashboard{% endblock %}

{% block body %}
<!-- start: page -->
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8\">
\t\t\t\t\t\t\t\t\t\t<div class=\"chart-data-selector\" id=\"salesSelectorWrapper\">
\t\t\t\t\t\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t\t\t\t\t\tSales:
\t\t\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"salesSelector\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Admin\" selected>JSOFT Admin</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Drupal\" >JSOFT Drupal</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Wordpress\" >JSOFT Wordpress</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t\t</h2>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"salesSelectorItems\" class=\"chart-data-selector-items mt-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Admin -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Admin\" id=\"flotDashSales1\" class=\"chart-active\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales1Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 190],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 320],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 270],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 180]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#0088cc\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Drupal -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Drupal\" id=\"flotDashSales2\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales2Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 290],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 540],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 480],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 220],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 170],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 190]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#2baab1\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Wordpress -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Wordpress\" id=\"flotDashSales3\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales3Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 840],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 740],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 690],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 940],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 1180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 820],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 570],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 780]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#734ba9\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 text-center\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title mt-md\">Sales Goal</h2>
\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-wrapper liquid-meter-sm mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter\">
\t\t\t\t\t\t\t\t\t\t\t\t<meter min=\"0\" max=\"100\" value=\"35\" id=\"meterSales\"></meter>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-selector\" id=\"meterSalesSel\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"35\" class=\"active\">Monthly Goal</a>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"28\">Annual Goal</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-primary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-life-ring\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Support Questions</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">1281</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-primary\">(14 unread)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(view all)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-secondary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-secondary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-usd\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Total Profit</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">\$ 14,890.30</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(withdraw)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-tertiary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-tertiary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Orders</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">38</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(statement)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-quartenary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-quartenary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Visitors</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">3765</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(report)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Profile</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-group\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading bg-primary\">

\t\t\t\t\t\t\t\t\t\t<div class=\"widget-profile-info\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name text-semibold\">John Doe</h4>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"role\">Administrator</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">(edit profile)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div id=\"accordion\">
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion panel-accordion-first\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1One\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check\"></i> Tasks
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1One\" class=\"accordion-body collapse in\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"widget-todo-list\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"\" id=\"todoListItem1\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem1\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem2\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem2\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem3\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem3\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem4\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem4\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem5\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem5\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem6\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem6\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr class=\"solid mt-sm mb-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<form method=\"get\" class=\"form-horizontal form-bordered\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" tabindex=\"-1\">Add</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1Two\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"fa fa-comment\"></i> Messages
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1Two\" class=\"accordion-body collapse\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list mb-xlg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineBarDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">488</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Average Profile Visits</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar circular-bar-xs m-none mt-xs mr-md pull-right\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar-chart\" data-percent=\"45\" data-plugin-options='{ \"barColor\": \"#2baab1\", \"delay\": 300, \"size\": 50, \"lineWidth\": 4 }'>
\t\t\t\t\t\t\t\t\t\t\t\t<strong>Average</strong>
\t\t\t\t\t\t\t\t\t\t\t\t<label><span class=\"percent\">45</span>%</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">12</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Working Projects</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineLineDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">89</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Pending Tasks</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t<span class=\"label label-primary label-sm text-normal va-middle mr-sm\">198</span>
\t\t\t\t\t\t\t\t\t<span class=\"va-middle\">Friends</span>
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<hr class=\"dotted short\">
\t\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t<a class=\"text-uppercase text-muted\" href=\"#\">(View All)</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"panel-footer\">
\t\t\t\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-6 col-lg-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Company Activity</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"timeline timeline-simple mt-xlg mb-md\">
\t\t\t\t\t\t\t\t\t<div class=\"tm-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"tm-title\">
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"h5 text-uppercase\">November 2013</h3>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<ol class=\"tm-items\">
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tIt's awesome when we find a good solution for our projects, JSOFT Admin is <span class=\"text-primary\">#awesome</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tCheckout! How cool is that!
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"thumbnail-gallery\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"img-thumbnail lightbox\" href=\"assets/images/projects/project-4.jpg\" data-plugin-options='{ \"type\":\"image\" }'>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" width=\"215\" src=\"assets/images/projects/project-4.jpg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"zoom\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Global Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div id=\"vectorMapWorld\" style=\"height: 350px; width: 100%;\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Projects Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-striped mb-none\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Project</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Progress</th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>1</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>2</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>3</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t60%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>4</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive Business WordPress Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t90%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>5</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Admin HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 45%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t45%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>6</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-danger\">Danger</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t40%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>7</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 95%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t95%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- end: page -->
{% endblock %}
{% block javascripts %}
\t<script src=\"{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.pie.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.categories.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.resize.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-sparkline/jquery.sparkline.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/raphael/raphael.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/morris/morris.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/gauge/gauge.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/snap-svg/snap.svg.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/liquid-meter/liquid.meter.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/jquery.vmap.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}\"></script>
{% endblock %}", "dashboard/index.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/dashboard/index.html.twig");
    }
}
