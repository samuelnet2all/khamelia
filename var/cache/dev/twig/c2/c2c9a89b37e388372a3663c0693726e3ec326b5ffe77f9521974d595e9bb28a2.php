<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* enseigne/membres.html.twig */
class __TwigTemplate_ff42d61a47badb75297a8dbddb9fd356b43635d45c21990d719b4a71b5da65b9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'position' => [$this, 'block_position'],
            'chemin' => [$this, 'block_chemin'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "enseigne/membres.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "enseigne/membres.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "enseigne/membres.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "KhaMELIA - Enseigne/Membres";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "\t
\t<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/css/datatables.css"), "html", null, true);
        echo " \" />
\t<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.css"), "html", null, true);
        echo " \" />
\t<link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/css/basic.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/css/dropzone.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote-bs3.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/lib/codemirror.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/theme/monokai.css"), "html", null, true);
        echo "\" />

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 25
    public function block_position($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        // line 26
        echo "\t";
        echo twig_escape_filter($this->env, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 26, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 29
    public function block_chemin($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        // line 30
        echo "\t";
        echo twig_escape_filter($this->env, (isset($context["chemin"]) || array_key_exists("chemin", $context) ? $context["chemin"] : (function () { throw new RuntimeError('Variable "chemin" does not exist.', 30, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 33
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 34
        echo "\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t<section class=\"panel\">
\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t</div>
\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\tListe des membres
\t\t\t\t\t</h2>
\t\t\t\t</header>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"center\">Nom</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Prénoms</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Email</th>
\t\t\t\t\t\t\t\t<th class=\"\">Profiles</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Roles</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Actions</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
        // line 58
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usersJoined"]) || array_key_exists("usersJoined", $context) ? $context["usersJoined"] : (function () { throw new RuntimeError('Variable "usersJoined" does not exist.', 58, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["userItem"]) {
            // line 59
            echo "\t\t\t\t\t\t\t\t<tr class=\"gradeX\">
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 61
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["usersArray"]) || array_key_exists("usersArray", $context) ? $context["usersArray"] : (function () { throw new RuntimeError('Variable "usersArray" does not exist.', 61, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["userItem"], "idUser", [], "any", false, false, false, 61), [], "array", false, false, false, 61), "nom", [], "any", false, false, false, 61), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 64
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["usersArray"]) || array_key_exists("usersArray", $context) ? $context["usersArray"] : (function () { throw new RuntimeError('Variable "usersArray" does not exist.', 64, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["userItem"], "idUser", [], "any", false, false, false, 64), [], "array", false, false, false, 64), "prenom", [], "any", false, false, false, 64), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["usersArray"]) || array_key_exists("usersArray", $context) ? $context["usersArray"] : (function () { throw new RuntimeError('Variable "usersArray" does not exist.', 67, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["userItem"], "idUser", [], "any", false, false, false, 67), [], "array", false, false, false, 67), "email", [], "any", false, false, false, 67), "html", null, true);
            echo "
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"pl-md left\">
\t\t\t\t\t\t\t\t\t\t";
            // line 70
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 70));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 71
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if ((0 === twig_compare($context["item"], "patient"))) {
                    // line 72
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Patient</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 73
$context["item"], "parent"))) {
                    // line 74
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Parent</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 75
$context["item"], "medecin"))) {
                    // line 76
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Médecin</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 77
$context["item"], "gardien"))) {
                    // line 78
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Gardien</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 79
$context["item"], "secretaire"))) {
                    // line 80
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Secrétaire</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 81
$context["item"], "laborantin"))) {
                    // line 82
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Laborantin</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 83
$context["item"], "infirmier"))) {
                    // line 84
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Infirmier</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 85
$context["item"], "assistant"))) {
                    // line 86
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Assistant</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 87
$context["item"], "vendeur"))) {
                    // line 88
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Vendeur</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 89
$context["item"], "delegue_medical"))) {
                    // line 90
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Délmégué médical</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 91
$context["item"], "accueil"))) {
                    // line 92
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Accueil</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 93
$context["item"], "directeur_general"))) {
                    // line 94
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Directeur général</li>
\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ((0 === twig_compare(                // line 95
$context["item"], "fondateur"))) {
                    // line 96
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Fondateur</li>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 98
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 99
                echo "\t\t\t\t\t\t\t\t\t\t\tnéant
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 101
            echo "\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"pl-md left\">
\t\t\t\t\t\t\t\t\t\t";
            // line 103
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["userItem"], "droits", [], "any", false, false, false, 103));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 104
                echo "\t\t\t\t\t\t\t\t\t\t\t";
                if ((0 === twig_compare($context["item"], "co_administrateur"))) {
                    // line 105
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<li>Co adminitrateur</li>
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 107
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 108
                echo "\t\t\t\t\t\t\t\t\t\t\tnéant
\t\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 110
            echo "\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#modalForm-";
            // line 112
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "id", [], "any", false, false, false, 112), "html", null, true);
            echo "\" class=\"modal-with-form btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-plus\"></span>
\t\t\t\t\t\t\t\t\t\t\tProfiles
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<div id=\"modalForm-";
            // line 116
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "id", [], "any", false, false, false, 116), "html", null, true);
            echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Ajout de profiles</h2>
\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"";
            // line 124
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile");
            echo "\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"";
            // line 125
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("profiles-item" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 125, $this->source); })()), "user", [], "any", false, false, false, 125), "id", [], "any", false, false, false, 125))), "html", null, true);
            echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_user\" value=\"";
            // line 126
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "idUser", [], "any", false, false, false, 126), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_enseigne\" value=\"";
            // line 127
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "idEnseigne", [], "any", false, false, false, 127), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-md-3 control-label\">Profiles</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" multiple=\"multiple\" data-plugin-multiselect data-plugin-options='{ \"enableCaseInsensitiveFiltering\": true }' id=\"ms_example6\" name=\"profiles[]\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"PUBLIC\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"patient\" ";
            // line 134
            if (twig_in_filter("patient", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 134))) {
                echo " selected=\"selected\" ";
            }
            echo ">Patient</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"parent\" ";
            // line 136
            if (twig_in_filter("parent", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 136))) {
                echo " selected=\"selected\" ";
            }
            echo ">Parent/accompagnateur</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"medecin\" ";
            // line 138
            if (twig_in_filter("medecin", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 138))) {
                echo " selected=\"selected\" ";
            }
            echo ">Médecin</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laborantin\" ";
            // line 140
            if (twig_in_filter("laborantin", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 140))) {
                echo " selected=\"selected\" ";
            }
            echo ">Laborantin</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"vendeur\" ";
            // line 142
            if (twig_in_filter("vendeur", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 142))) {
                echo " selected=\"selected\" ";
            }
            echo ">Vendeur</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"delegue_medical\" ";
            // line 144
            if (twig_in_filter("delegue_medical", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 144))) {
                echo " selected=\"selected\" ";
            }
            echo ">Délégué médical</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"ADMINISTRATION\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"gardien\" ";
            // line 148
            if (twig_in_filter("gardien", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 148))) {
                echo " selected=\"selected\" ";
            }
            echo ">Gardien</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"secretaire\" ";
            // line 150
            if (twig_in_filter("secretaire", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 150))) {
                echo " selected=\"selected\" ";
            }
            echo ">Secrétaire</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"accueil\" ";
            // line 152
            if (twig_in_filter("accueil", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 152))) {
                echo " selected=\"selected\" ";
            }
            echo ">Accueil</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"assistant\" ";
            // line 154
            if (twig_in_filter("assistant", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 154))) {
                echo " selected=\"selected\" ";
            }
            echo ">Assistant</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"infirmier\" ";
            // line 156
            if (twig_in_filter("infirmier", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 156))) {
                echo " selected=\"selected\" ";
            }
            echo " >Infirmier</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"directeur_general\" ";
            // line 158
            if (twig_in_filter("directeur_general", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 158))) {
                echo " selected=\"selected\" ";
            }
            echo " >Directeur Général</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"fondateur\" ";
            // line 160
            if (twig_in_filter("fondateur", twig_get_attribute($this->env, $this->source, $context["userItem"], "profiles", [], "any", false, false, false, 160))) {
                echo " selected=\"selected\" ";
            }
            echo " >Fondateur</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary\" value=\"Enrégistrer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<a href=\"#modalFormDroit-";
            // line 184
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "id", [], "any", false, false, false, 184), "html", null, true);
            echo "\" class=\"modal-with-form btn btn-dark\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-info\"></span>
\t\t\t\t\t\t\t\t\t\t\tDroits
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<div id=\"modalFormDroit-";
            // line 188
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "id", [], "any", false, false, false, 188), "html", null, true);
            echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Ajout de droit(s)</h2>
\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"";
            // line 196
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("droits");
            echo "\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"";
            // line 197
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("droits-item" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 197, $this->source); })()), "user", [], "any", false, false, false, 197), "id", [], "any", false, false, false, 197))), "html", null, true);
            echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_user\" value=\"";
            // line 198
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "idUser", [], "any", false, false, false, 198), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_enseigne\" value=\"";
            // line 199
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["userItem"], "idEnseigne", [], "any", false, false, false, 199), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-md-3 control-label\">Droits</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" multiple=\"multiple\" data-plugin-multiselect data-plugin-options='{ \"enableCaseInsensitiveFiltering\": true }' id=\"ms_example6\" name=\"droits[]\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"co_administrateur\" ";
            // line 204
            if (twig_in_filter("co_administrateur", twig_get_attribute($this->env, $this->source, $context["userItem"], "droits", [], "any", false, false, false, 204))) {
                echo " selected=\"selected\" ";
            }
            echo ">Co-administrateur</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary\" value=\"Enrégistrer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['userItem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 228
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 236
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 237
        echo "\t
\t<script src=\"";
        // line 238
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 239
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 240
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 241
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 242
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 243
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/ui-elements/examples.modals.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 244
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/forms/examples.advanced.form.js"), "html", null, true);
        echo "\" /></script>

\t<script src=\"";
        // line 246
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 247
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 248
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 249
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 250
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-maskedinput/jquery.maskedinput.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 251
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 252
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 253
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 254
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/fuelux/js/spinner.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/dropzone/dropzone.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-markdown/js/markdown.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 257
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-markdown/js/to-markdown.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 258
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 259
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/lib/codemirror.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 260
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/addon/selection/active-line.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 261
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/addon/edit/matchbrackets.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 262
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/mode/javascript/javascript.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 263
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/mode/xml/xml.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 264
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 265
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/codemirror/mode/css/css.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 266
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/summernote/summernote.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 267
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 268
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/ios7-switch/ios7-switch.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 269
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/tables/examples.datatables.default.js"), "html", null, true);
        echo "\"></script>

\t\t
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "enseigne/membres.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  767 => 269,  763 => 268,  759 => 267,  755 => 266,  751 => 265,  747 => 264,  743 => 263,  739 => 262,  735 => 261,  731 => 260,  727 => 259,  723 => 258,  719 => 257,  715 => 256,  711 => 255,  707 => 254,  703 => 253,  699 => 252,  695 => 251,  691 => 250,  687 => 249,  683 => 248,  679 => 247,  675 => 246,  670 => 244,  666 => 243,  662 => 242,  658 => 241,  654 => 240,  650 => 239,  646 => 238,  643 => 237,  633 => 236,  617 => 228,  585 => 204,  577 => 199,  573 => 198,  569 => 197,  565 => 196,  554 => 188,  547 => 184,  518 => 160,  511 => 158,  504 => 156,  497 => 154,  490 => 152,  483 => 150,  476 => 148,  467 => 144,  460 => 142,  453 => 140,  446 => 138,  439 => 136,  432 => 134,  422 => 127,  418 => 126,  414 => 125,  410 => 124,  399 => 116,  392 => 112,  388 => 110,  381 => 108,  376 => 107,  372 => 105,  369 => 104,  364 => 103,  360 => 101,  353 => 99,  348 => 98,  344 => 96,  342 => 95,  339 => 94,  337 => 93,  334 => 92,  332 => 91,  329 => 90,  327 => 89,  324 => 88,  322 => 87,  319 => 86,  317 => 85,  314 => 84,  312 => 83,  309 => 82,  307 => 81,  304 => 80,  302 => 79,  299 => 78,  297 => 77,  294 => 76,  292 => 75,  289 => 74,  287 => 73,  284 => 72,  281 => 71,  276 => 70,  270 => 67,  264 => 64,  258 => 61,  254 => 59,  250 => 58,  224 => 34,  214 => 33,  201 => 30,  191 => 29,  178 => 26,  168 => 25,  155 => 21,  151 => 20,  147 => 19,  143 => 18,  139 => 17,  135 => 16,  131 => 15,  127 => 14,  123 => 13,  119 => 12,  115 => 11,  111 => 10,  107 => 9,  103 => 8,  99 => 7,  95 => 6,  92 => 5,  82 => 4,  63 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}KhaMELIA - Enseigne/Membres{% endblock %}
{% block stylesheets %}
\t
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/select2/select2.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }} \" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/pnotify/pnotify.custom.css') }} \" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/select2/select2.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/dropzone/css/basic.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/dropzone/css/dropzone.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-markdown/css/bootstrap-markdown.min.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/summernote/summernote.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/summernote/summernote-bs3.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/codemirror/lib/codemirror.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/codemirror/theme/monokai.css') }}\" />

{% endblock %}

{% block position %}
\t{{ position }}
{% endblock %}

{% block chemin %}
\t{{ chemin }}
{% endblock %}

{% block body %}
\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t<section class=\"panel\">
\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t</div>
\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\tListe des membres
\t\t\t\t\t</h2>
\t\t\t\t</header>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"center\">Nom</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Prénoms</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Email</th>
\t\t\t\t\t\t\t\t<th class=\"\">Profiles</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Roles</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Actions</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t{% for userItem in usersJoined %}
\t\t\t\t\t\t\t\t<tr class=\"gradeX\">
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t{{ usersArray[userItem.idUser].nom }}
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t{{ usersArray[userItem.idUser].prenom }}
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t{{ usersArray[userItem.idUser].email }}
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"pl-md left\">
\t\t\t\t\t\t\t\t\t\t{% for item in userItem.profiles %}
\t\t\t\t\t\t\t\t\t\t\t{% if item == 'patient' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Patient</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'parent' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Parent</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'medecin' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Médecin</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'gardien' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Gardien</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'secretaire' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Secrétaire</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'laborantin' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Laborantin</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'infirmier' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Infirmier</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'assistant' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Assistant</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'vendeur' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Vendeur</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'delegue_medical' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Délmégué médical</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'accueil' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Accueil</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'directeur_general' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Directeur général</li>
\t\t\t\t\t\t\t\t\t\t\t{% elseif item == 'fondateur' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Fondateur</li>
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\tnéant
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"pl-md left\">
\t\t\t\t\t\t\t\t\t\t{% for item in userItem.droits %}
\t\t\t\t\t\t\t\t\t\t\t{% if item == 'co_administrateur' %}
\t\t\t\t\t\t\t\t\t\t\t\t<li>Co adminitrateur</li>
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\tnéant
\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t<a href=\"#modalForm-{{ userItem.id }}\" class=\"modal-with-form btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-plus\"></span>
\t\t\t\t\t\t\t\t\t\t\tProfiles
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<div id=\"modalForm-{{ userItem.id }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Ajout de profiles</h2>
\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"{{ path('profile') }}\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('profiles-item'~ app.user.id) }}\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_user\" value=\"{{ userItem.idUser }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_enseigne\" value=\"{{ userItem.idEnseigne }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-md-3 control-label\">Profiles</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" multiple=\"multiple\" data-plugin-multiselect data-plugin-options='{ \"enableCaseInsensitiveFiltering\": true }' id=\"ms_example6\" name=\"profiles[]\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"PUBLIC\">

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"patient\" {% if \"patient\" in userItem.profiles %} selected=\"selected\" {% endif %}>Patient</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"parent\" {% if \"parent\" in userItem.profiles %} selected=\"selected\" {% endif %}>Parent/accompagnateur</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"medecin\" {% if \"medecin\" in userItem.profiles %} selected=\"selected\" {% endif %}>Médecin</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laborantin\" {% if \"laborantin\" in userItem.profiles %} selected=\"selected\" {% endif %}>Laborantin</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"vendeur\" {% if \"vendeur\" in userItem.profiles %} selected=\"selected\" {% endif %}>Vendeur</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"delegue_medical\" {% if \"delegue_medical\" in userItem.profiles %} selected=\"selected\" {% endif %}>Délégué médical</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<optgroup label=\"ADMINISTRATION\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"gardien\" {% if \"gardien\" in userItem.profiles %} selected=\"selected\" {% endif %}>Gardien</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"secretaire\" {% if \"secretaire\" in userItem.profiles %} selected=\"selected\" {% endif %}>Secrétaire</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"accueil\" {% if \"accueil\" in userItem.profiles %} selected=\"selected\" {% endif %}>Accueil</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"assistant\" {% if \"assistant\" in userItem.profiles %} selected=\"selected\" {% endif %}>Assistant</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"infirmier\" {% if \"infirmier\" in userItem.profiles %} selected=\"selected\" {% endif %} >Infirmier</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"directeur_general\" {% if \"directeur_general\" in userItem.profiles %} selected=\"selected\" {% endif %} >Directeur Général</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"fondateur\" {% if \"fondateur\" in userItem.profiles %} selected=\"selected\" {% endif %} >Fondateur</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</optgroup>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary\" value=\"Enrégistrer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<a href=\"#modalFormDroit-{{ userItem.id }}\" class=\"modal-with-form btn btn-dark\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-info\"></span>
\t\t\t\t\t\t\t\t\t\t\tDroits
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t<div id=\"modalFormDroit-{{ userItem.id }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Ajout de droit(s)</h2>
\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"{{ path('droits') }}\" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('droits-item'~ app.user.id) }}\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_user\" value=\"{{ userItem.idUser }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"id_enseigne\" value=\"{{ userItem.idEnseigne }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-md-3 control-label\">Droits</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" multiple=\"multiple\" data-plugin-multiselect data-plugin-options='{ \"enableCaseInsensitiveFiltering\": true }' id=\"ms_example6\" name=\"droits[]\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"co_administrateur\" {% if \"co_administrateur\" in userItem.droits %} selected=\"selected\" {% endif %}>Co-administrateur</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" class=\"btn btn-primary\" value=\"Enrégistrer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</section>
\t\t</div>
\t</div>
{% endblock %}

{% block javascripts %}
\t
\t<script src=\"{{ asset('assets/vendor/select2/select2.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}\"></script>
\t<script src=\"{{ asset('assets/javascripts/ui-elements/examples.modals.js') }}\"></script>
\t<script src=\"{{ asset('assets/javascripts/forms/examples.advanced.form.js') }}\" /></script>

\t<script src=\"{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/select2/select2.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/fuelux/js/spinner.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/dropzone/dropzone.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-markdown/js/markdown.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-markdown/js/to-markdown.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/lib/codemirror.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/addon/selection/active-line.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/addon/edit/matchbrackets.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/mode/javascript/javascript.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/mode/xml/xml.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/codemirror/mode/css/css.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/summernote/summernote.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/ios7-switch/ios7-switch.js') }}\"></script>
\t<script src=\"{{ asset('assets/javascripts/tables/examples.datatables.default.js') }}\"></script>

\t\t
{% endblock %}", "enseigne/membres.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/enseigne/membres.html.twig");
    }
}
