<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* enseigne/index.html.twig */
class __TwigTemplate_5762a3b42da0b1b3c91cfcada8cce34e0e9d6ae86eda83eb2aefed048d93166a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'position' => [$this, 'block_position'],
            'chemin' => [$this, 'block_chemin'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "enseigne/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "enseigne/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "enseigne/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "KhaMELIA - Enseigne";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "\t
\t<link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/css/datatables.css"), "html", null, true);
        echo " \" />
\t<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.css"), "html", null, true);
        echo " \" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 11
    public function block_position($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        // line 12
        echo "\t";
        echo twig_escape_filter($this->env, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 12, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 15
    public function block_chemin($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        // line 16
        echo "\t";
        echo twig_escape_filter($this->env, (isset($context["chemin"]) || array_key_exists("chemin", $context) ? $context["chemin"] : (function () { throw new RuntimeError('Variable "chemin" does not exist.', 16, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 19
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 20
        echo "\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t<section class=\"panel\">
\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t";
        // line 30
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 30, $this->source); })()), "user", [], "any", false, false, false, 30), "type", [], "any", false, false, false, 30), (isset($context["TYPE_PARTICULIER"]) || array_key_exists("TYPE_PARTICULIER", $context) ? $context["TYPE_PARTICULIER"] : (function () { throw new RuntimeError('Variable "TYPE_PARTICULIER" does not exist.', 30, $this->source); })())))) {
            // line 31
            echo "\t\t\t\t\t\t\tEnseignes de KhaMELIA
\t\t\t\t\t\t";
        } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 32
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "user", [], "any", false, false, false, 32), "type", [], "any", false, false, false, 32), (isset($context["TYPE_ENTREPRISE"]) || array_key_exists("TYPE_ENTREPRISE", $context) ? $context["TYPE_ENTREPRISE"] : (function () { throw new RuntimeError('Variable "TYPE_ENTREPRISE" does not exist.', 32, $this->source); })())))) {
            // line 33
            echo "\t\t\t\t\t\t\tVos enseignes créées
\t\t\t\t\t\t\t<a href=\"#modalFormCreate\" class=\"modal-with-form mb-xs mt-xs mr-xs btn btn-success\" >
\t\t\t\t\t\t\t\t<span class=\"fa fa-plus\"></span>
\t\t\t\t\t\t\t\tCréer
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<!-- Modal Form -->
\t\t\t\t\t\t\t<div id=\"modalFormCreate\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Création d'une enseigne</h2>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<form action=\"";
            // line 45
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("create_enseigne");
            echo " \" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("create-item" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 46, $this->source); })()), "user", [], "any", false, false, false, 46), "id", [], "any", false, false, false, 46))), "html", null, true);
            echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" value=\"";
            // line 47
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "user", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47), "html", null, true);
            echo "\" name=\"id_entreprise\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Nom de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"nom_enseigne\" class=\"form-control\" placeholder=\"Entrez le nom de l'enseigne...\" required />
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Code de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"code_enseigne\" class=\"form-control\" placeholder=\"Entrez le code de l'enseigne...\" required />
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-right pr-md\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Créer\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t";
        }
        // line 78
        echo "\t\t\t\t\t</h2>

\t\t\t\t</header>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"center\">Nom de l'enseigne</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Code de l'enseigne</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Type</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Action(s)</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t";
        // line 92
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 92, $this->source); })()), "user", [], "any", false, false, false, 92), "type", [], "any", false, false, false, 92), (isset($context["TYPE_PARTICULIER"]) || array_key_exists("TYPE_PARTICULIER", $context) ? $context["TYPE_PARTICULIER"] : (function () { throw new RuntimeError('Variable "TYPE_PARTICULIER" does not exist.', 92, $this->source); })())))) {
            // line 93
            echo "\t\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["affiliées"]) || array_key_exists("affiliées", $context) ? $context["affiliées"] : (function () { throw new RuntimeError('Variable "affiliées" does not exist.', 93, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["enseigneAffiliee"]) {
                // line 94
                echo "\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 96
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "nom_enseigne", [], "any", false, false, false, 96), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 99
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "code_enseigne", [], "any", false, false, false, 99), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t\t";
                // line 102
                if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "id_enseigne", [], "any", false, false, false, 102), (isset($context["joined"]) || array_key_exists("joined", $context) ? $context["joined"] : (function () { throw new RuntimeError('Variable "joined" does not exist.', 102, $this->source); })()))) {
                    // line 103
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "id_enseigne", [], "any", false, false, false, 103), "html", null, true);
                    echo "\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-reply \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tRejoindre
\t\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-";
                    // line 108
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "id_enseigne", [], "any", false, false, false, 108), "html", null, true);
                    echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir rejoindre cette enseigne ?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 127
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rejoindre", ["id_enseigne" => twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "id_enseigne", [], "any", false, false, false, 127)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 137
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    $context["coadmin1"] = false;
                    // line 138
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["enseignesJoined"]) || array_key_exists("enseignesJoined", $context) ? $context["enseignesJoined"] : (function () { throw new RuntimeError('Variable "enseignesJoined" does not exist.', 138, $this->source); })()));
                    foreach ($context['_seq'] as $context["_key"] => $context["enseigneJoind"]) {
                        // line 139
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "idUser", [], "any", false, false, false, 139), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 139, $this->source); })()), "user", [], "any", false, false, false, 139), "id", [], "any", false, false, false, 139))) && twig_in_filter("co_administrateur", twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "droits", [], "any", false, false, false, 139))) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "idEnseigne", [], "any", false, false, false, 139), twig_get_attribute($this->env, $this->source, $context["enseigneAffiliee"], "id_enseigne", [], "any", false, false, false, 139))))) {
                            // line 140
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("membre", ["id_enseigne" => twig_get_attribute($this->env, $this->source, (isset($context["enseigne"]) || array_key_exists("enseigne", $context) ? $context["enseigne"] : (function () { throw new RuntimeError('Variable "enseigne" does not exist.', 140, $this->source); })()), "id_enseigne", [], "any", false, false, false, 140)]), "html", null, true);
                            echo "\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            // line 144
                            $context["coadmin1"] = true;
                            // line 145
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 146
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseigneJoind'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 147
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ((0 === twig_compare((isset($context["coadmin1"]) || array_key_exists("coadmin1", $context) ? $context["coadmin1"] : (function () { throw new RuntimeError('Variable "coadmin1" does not exist.', 147, $this->source); })()), false))) {
                        // line 148
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\tAucune(s)
\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 150
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 154
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 155
                echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseigneAffiliee'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 158
            echo "\t\t\t\t\t\t\t";
        } elseif ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 158, $this->source); })()), "user", [], "any", false, false, false, 158), "type", [], "any", false, false, false, 158), (isset($context["TYPE_ENTREPRISE"]) || array_key_exists("TYPE_ENTREPRISE", $context) ? $context["TYPE_ENTREPRISE"] : (function () { throw new RuntimeError('Variable "TYPE_ENTREPRISE" does not exist.', 158, $this->source); })())))) {
            // line 159
            echo "\t\t\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enseignes"]) || array_key_exists("enseignes", $context) ? $context["enseignes"] : (function () { throw new RuntimeError('Variable "enseignes" does not exist.', 159, $this->source); })()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["enseigne"]) {
                // line 160
                echo "\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 162
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "nom_enseigne", [], "any", false, false, false, 162), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 165
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "code_enseigne", [], "any", false, false, false, 165), "html", null, true);
                echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                // line 168
                if (( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 168, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 168), [], "array", true, true, false, 168))) {
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 169
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 169, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 169), [], "array", false, false, false, 169), "html", null, true);
                    echo " 
\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 171
                    echo "\t\t\t\t\t\t\t\t\t\t\t\tpas encore définis
\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 173
                echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t";
                // line 175
                if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 175), (isset($context["affiliées"]) || array_key_exists("affiliées", $context) ? $context["affiliées"] : (function () { throw new RuntimeError('Variable "affiliées" does not exist.', 175, $this->source); })()))) {
                    // line 176
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 176), "html", null, true);
                    echo "\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-plus \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tAffilier
\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-";
                    // line 181
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 181), "html", null, true);
                    echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir affilier cette enseigne à KhaMELIA?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 200
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("affilier", ["id_enseigne" => twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 200)]), "html", null, true);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 210
                    echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("membre", ["id_enseigne" => twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 210)]), "html", null, true);
                    echo " \" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t";
                }
                // line 215
                echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalFormEdit-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 215), "html", null, true);
                echo "\" class=\"modal-with-form mb-xs mt-xs mr-xs btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-edit\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tEditer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<!-- Modal Form -->
\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalFormEdit-";
                // line 221
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 221), "html", null, true);
                echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Mise à jour de l'enseigne</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"";
                // line 227
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("edit_enseigne");
                echo " \" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"";
                // line 228
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("edit-item" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 228, $this->source); })()), "user", [], "any", false, false, false, 228), "id", [], "any", false, false, false, 228))), "html", null, true);
                echo "\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" value=\"";
                // line 229
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 229), "html", null, true);
                echo "\" name=\"id_enseigne\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Nom de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"nom_enseigne\" class=\"form-control\" placeholder=\"Entrez le nom de l'enseigne...\" required value=\"";
                // line 233
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "nom_enseigne", [], "any", false, false, false, 233), "html", null, true);
                echo " \" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Code de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"code_enseigne\" class=\"form-control\" placeholder=\"Entrez le code de l'enseigne...\" required value=\"";
                // line 239
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "code_enseigne", [], "any", false, false, false, 239), "html", null, true);
                echo " \" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Type</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"ms_example6\" name=\"type\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"centre_sante\" ";
                // line 246
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 246, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 246), [], "array", true, true, false, 246)) && (0 === twig_compare("centre_sante", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 246, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 246), [], "array", false, false, false, 246))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Centre de santé</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"pharmacie\" ";
                // line 248
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 248, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 248), [], "array", true, true, false, 248)) && (0 === twig_compare("pharmacie", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 248, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 248), [], "array", false, false, false, 248))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Pharmacie</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laboratoire_analyse\" ";
                // line 250
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 250, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 250), [], "array", true, true, false, 250)) && (0 === twig_compare("laboratoire_analyse", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 250, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 250), [], "array", false, false, false, 250))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Laboratoire d'analyse</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"maternite\" ";
                // line 252
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 252, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 252), [], "array", true, true, false, 252)) && (0 === twig_compare("maternite", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 252, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 252), [], "array", false, false, false, 252))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Maternité</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"centre_vaccination\" ";
                // line 254
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 254, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 254), [], "array", true, true, false, 254)) && (0 === twig_compare("centre_vaccination", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 254, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 254), [], "array", false, false, false, 254))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Centre de vaccination</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"assurance\" ";
                // line 256
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 256, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 256), [], "array", true, true, false, 256)) && (0 === twig_compare("assurance", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 256, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 256), [], "array", false, false, false, 256))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Assurance</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laboratoire_pharmaceutique\" ";
                // line 258
                if ((( !twig_test_empty((isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 258, $this->source); })())) && twig_get_attribute($this->env, $this->source, ($context["enseignesTypes"] ?? null), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 258), [], "array", true, true, false, 258)) && (0 === twig_compare("laboratoire_pharmaceutique", twig_get_attribute($this->env, $this->source, (isset($context["enseignesTypes"]) || array_key_exists("enseignesTypes", $context) ? $context["enseignesTypes"] : (function () { throw new RuntimeError('Variable "enseignesTypes" does not exist.', 258, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 258), [], "array", false, false, false, 258))))) {
                    echo " selected=\"selected\" ";
                }
                echo ">Laboratoire Pharmaceutique</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-right pr-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Mettre à jour\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Cancel</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 281
                echo "\t\t\t\t\t\t\t\t\t<tr class=\"gradeX\" colspan=\"4\"> Vous n'avez créé aucune enseigne</tr>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseigne'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 283
            echo "\t\t\t\t\t\t\t";
        }
        // line 284
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</section>
\t\t\t";
        // line 288
        if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 288, $this->source); })()), "user", [], "any", false, false, false, 288), "type", [], "any", false, false, false, 288), (isset($context["TYPE_ENTREPRISE"]) || array_key_exists("TYPE_ENTREPRISE", $context) ? $context["TYPE_ENTREPRISE"] : (function () { throw new RuntimeError('Variable "TYPE_ENTREPRISE" does not exist.', 288, $this->source); })())))) {
            // line 289
            echo "\t\t\t\t<section class=\"panel\">
\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\tEnseignes de KhaMELIA
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t
\t\t\t\t\t</header>
\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Nom de l'enseigne</th>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Code de l'enseigne</th>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Action(s)</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody class=\"text-center\">
\t\t\t\t\t\t\t";
            // line 310
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["enseignesAffiliees"]) || array_key_exists("enseignesAffiliees", $context) ? $context["enseignesAffiliees"] : (function () { throw new RuntimeError('Variable "enseignesAffiliees" does not exist.', 310, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["enseigne"]) {
                // line 311
                echo "\t\t\t\t\t\t\t\t";
                if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 311), (isset($context["myEnseignesId"]) || array_key_exists("myEnseignesId", $context) ? $context["myEnseignesId"] : (function () { throw new RuntimeError('Variable "myEnseignesId" does not exist.', 311, $this->source); })()))) {
                    // line 312
                    echo "\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 314
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "nom_enseigne", [], "any", false, false, false, 314), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 317
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "code_enseigne", [], "any", false, false, false, 317), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t\t";
                    // line 320
                    if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 320), (isset($context["joined"]) || array_key_exists("joined", $context) ? $context["joined"] : (function () { throw new RuntimeError('Variable "joined" does not exist.', 320, $this->source); })()))) {
                        // line 321
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 321), "html", null, true);
                        echo "\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-reply \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tRejoindre
\t\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-";
                        // line 326
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 326), "html", null, true);
                        echo "\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir rejoindre cette enseigne ?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 345
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("rejoindre", ["id_enseigne" => twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 345)]), "html", null, true);
                        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 355
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        $context["coadmin"] = false;
                        // line 356
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["enseignesJoined"]) || array_key_exists("enseignesJoined", $context) ? $context["enseignesJoined"] : (function () { throw new RuntimeError('Variable "enseignesJoined" does not exist.', 356, $this->source); })()));
                        foreach ($context['_seq'] as $context["_key"] => $context["enseigneJoind"]) {
                            // line 357
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            if ((((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "idUser", [], "any", false, false, false, 357), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 357, $this->source); })()), "user", [], "any", false, false, false, 357), "id", [], "any", false, false, false, 357))) && twig_in_filter("co_administrateur", twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "droits", [], "any", false, false, false, 357))) && (0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["enseigneJoind"], "idEnseigne", [], "any", false, false, false, 357), twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 357))))) {
                                // line 358
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("membre", ["id_enseigne" => twig_get_attribute($this->env, $this->source, $context["enseigne"], "id_enseigne", [], "any", false, false, false, 358)]), "html", null, true);
                                echo "\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                                // line 362
                                $context["coadmin"] = true;
                                // line 363
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            }
                            // line 364
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseigneJoind'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 365
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ((0 === twig_compare((isset($context["coadmin"]) || array_key_exists("coadmin", $context) ? $context["coadmin"] : (function () { throw new RuntimeError('Variable "coadmin" does not exist.', 365, $this->source); })()), false))) {
                            // line 366
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\tAucune(s)
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 368
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 372
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 373
                    echo "\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
                }
                // line 376
                echo "\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['enseigne'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 377
            echo "\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t";
        }
        // line 382
        echo "\t\t</div>
\t</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 386
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 387
        echo "\t
\t<script src=\"";
        // line 388
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 389
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 390
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 391
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"), "html", null, true);
        echo " \"></script>
\t<script src=\"";
        // line 392
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 393
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/ui-elements/examples.modals.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 394
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/tables/examples.datatables.default.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "enseigne/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  822 => 394,  818 => 393,  814 => 392,  810 => 391,  806 => 390,  802 => 389,  798 => 388,  795 => 387,  785 => 386,  773 => 382,  766 => 377,  760 => 376,  755 => 373,  752 => 372,  750 => 368,  746 => 366,  743 => 365,  737 => 364,  734 => 363,  732 => 362,  724 => 358,  721 => 357,  716 => 356,  713 => 355,  700 => 345,  678 => 326,  669 => 321,  667 => 320,  661 => 317,  655 => 314,  651 => 312,  648 => 311,  644 => 310,  621 => 289,  619 => 288,  613 => 284,  610 => 283,  603 => 281,  573 => 258,  566 => 256,  559 => 254,  552 => 252,  545 => 250,  538 => 248,  531 => 246,  521 => 239,  512 => 233,  505 => 229,  501 => 228,  497 => 227,  488 => 221,  478 => 215,  469 => 210,  456 => 200,  434 => 181,  425 => 176,  423 => 175,  419 => 173,  415 => 171,  410 => 169,  406 => 168,  400 => 165,  394 => 162,  390 => 160,  384 => 159,  381 => 158,  373 => 155,  370 => 154,  368 => 150,  364 => 148,  361 => 147,  355 => 146,  352 => 145,  350 => 144,  342 => 140,  339 => 139,  334 => 138,  331 => 137,  318 => 127,  296 => 108,  287 => 103,  285 => 102,  279 => 99,  273 => 96,  269 => 94,  264 => 93,  262 => 92,  246 => 78,  212 => 47,  208 => 46,  204 => 45,  190 => 33,  188 => 32,  185 => 31,  183 => 30,  171 => 20,  161 => 19,  148 => 16,  138 => 15,  125 => 12,  115 => 11,  103 => 8,  99 => 7,  95 => 6,  92 => 5,  82 => 4,  63 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}KhaMELIA - Enseigne{% endblock %}
{% block stylesheets %}
\t
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/select2/select2.css') }}\" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }} \" />
\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/pnotify/pnotify.custom.css') }} \" />
{% endblock %}

{% block position %}
\t{{ position }}
{% endblock %}

{% block chemin %}
\t{{ chemin }}
{% endblock %}

{% block body %}
\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t<section class=\"panel\">
\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t
\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t{% if app.user.type == TYPE_PARTICULIER %}
\t\t\t\t\t\t\tEnseignes de KhaMELIA
\t\t\t\t\t\t{% elseif app.user.type == TYPE_ENTREPRISE %}
\t\t\t\t\t\t\tVos enseignes créées
\t\t\t\t\t\t\t<a href=\"#modalFormCreate\" class=\"modal-with-form mb-xs mt-xs mr-xs btn btn-success\" >
\t\t\t\t\t\t\t\t<span class=\"fa fa-plus\"></span>
\t\t\t\t\t\t\t\tCréer
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<!-- Modal Form -->
\t\t\t\t\t\t\t<div id=\"modalFormCreate\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Création d'une enseigne</h2>
\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<form action=\"{{ path('create_enseigne') }} \" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('create-item'~ app.user.id) }}\"/>
\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" value=\"{{ app.user.id }}\" name=\"id_entreprise\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Nom de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"nom_enseigne\" class=\"form-control\" placeholder=\"Entrez le nom de l'enseigne...\" required />
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Code de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"code_enseigne\" class=\"form-control\" placeholder=\"Entrez le code de l'enseigne...\" required />
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-right pr-md\">
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Créer\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Annuler</button>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t{% endif %}
\t\t\t\t\t</h2>

\t\t\t\t</header>
\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th class=\"center\">Nom de l'enseigne</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Code de l'enseigne</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Type</th>
\t\t\t\t\t\t\t\t<th class=\"center\">Action(s)</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t{% if app.user.type == TYPE_PARTICULIER %}
\t\t\t\t\t\t\t\t{% for enseigneAffiliee in affiliées %}
\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigneAffiliee.nom_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigneAffiliee.code_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t\t{% if enseigneAffiliee.id_enseigne not in joined %}
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-{{ enseigneAffiliee.id_enseigne }}\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-reply \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tRejoindre
\t\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-{{ enseigneAffiliee.id_enseigne }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir rejoindre cette enseigne ?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('rejoindre', {'id_enseigne': enseigneAffiliee.id_enseigne}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t\t{% set coadmin1 = false %}
\t\t\t\t\t\t\t\t\t\t\t\t{% for enseigneJoind in enseignesJoined %}
\t\t\t\t\t\t\t\t\t\t\t\t\t{% if enseigneJoind.idUser == app.user.id and 'co_administrateur' in enseigneJoind.droits and enseigneJoind.idEnseigne == enseigneAffiliee.id_enseigne %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('membre', {'id_enseigne': enseigne.id_enseigne}) }}\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% set coadmin1 = true %}
\t\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t{% if coadmin1 == false %}
\t\t\t\t\t\t\t\t\t\t\t\t\tAucune(s)
\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t{#<a href=\"\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-info\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tMes détails
\t\t\t\t\t\t\t\t\t\t\t\t</a>#}
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t{% elseif app.user.type == TYPE_ENTREPRISE %}
\t\t\t\t\t\t\t\t{% for enseigne in enseignes %}
\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigne.nom_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigne.code_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined %} 
\t\t\t\t\t\t\t\t\t\t\t\t{{ enseignesTypes[enseigne.id_enseigne] }} 
\t\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t\tpas encore définis
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t{% if enseigne.id_enseigne not in affiliées %}
\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-{{ enseigne.id_enseigne }}\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-plus \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tAffilier
\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-{{ enseigne.id_enseigne }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir affilier cette enseigne à KhaMELIA?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('affilier', {'id_enseigne': enseigne.id_enseigne}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('membre', {'id_enseigne': enseigne.id_enseigne}) }} \" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalFormEdit-{{ enseigne.id_enseigne }}\" class=\"modal-with-form mb-xs mt-xs mr-xs btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-edit\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tEditer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<!-- Modal Form -->
\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalFormEdit-{{ enseigne.id_enseigne }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Mise à jour de l'enseigne</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<form action=\"{{ path('edit_enseigne') }} \" method=\"post\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" name=\"_token\" value=\"{{ csrf_token('edit-item'~ app.user.id) }}\"/>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" value=\"{{ enseigne.id_enseigne }}\" name=\"id_enseigne\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Nom de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"nom_enseigne\" class=\"form-control\" placeholder=\"Entrez le nom de l'enseigne...\" required value=\"{{enseigne.nom_enseigne }} \" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Code de l'enseigne</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"code_enseigne\" class=\"form-control\" placeholder=\"Entrez le code de l'enseigne...\" required value=\"{{ enseigne.code_enseigne }} \" />
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"col-sm-3 control-label\">Type</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-9\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"ms_example6\" name=\"type\" required=\"required\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"centre_sante\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"centre_sante\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Centre de santé</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"pharmacie\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"pharmacie\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Pharmacie</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laboratoire_analyse\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"laboratoire_analyse\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Laboratoire d'analyse</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"maternite\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"maternite\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Maternité</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"centre_vaccination\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"centre_vaccination\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Centre de vaccination</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"assurance\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"assurance\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Assurance</option>

\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"laboratoire_pharmaceutique\" {% if enseignesTypes is not empty and enseignesTypes[enseigne.id_enseigne] is defined and \"laboratoire_pharmaceutique\" == enseignesTypes[enseigne.id_enseigne] %} selected=\"selected\" {% endif %}>Laboratoire Pharmaceutique</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group text-right pr-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"submit\" value=\"Mettre à jour\" class=\"btn btn-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Cancel</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t<tr class=\"gradeX\" colspan=\"4\"> Vous n'avez créé aucune enseigne</tr>
\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</section>
\t\t\t{% if app.user.type == TYPE_ENTREPRISE %}
\t\t\t\t<section class=\"panel\">
\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t</div>
\t\t\t\t
\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\tEnseignes de KhaMELIA
\t\t\t\t\t\t</h2>
\t\t\t\t\t\t
\t\t\t\t\t</header>
\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t<table class=\"table table-bordered table-striped mb-none\" id=\"datatable-default\">
\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Nom de l'enseigne</th>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Code de l'enseigne</th>
\t\t\t\t\t\t\t\t\t<th class=\"text-center\">Action(s)</th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody class=\"text-center\">
\t\t\t\t\t\t\t{% for enseigne in enseignesAffiliees %}
\t\t\t\t\t\t\t\t{% if enseigne.id_enseigne not in myEnseignesId %}
\t\t\t\t\t\t\t\t\t<tr class=\"gradeX center\">
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigne.nom_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t{{ enseigne.code_enseigne }}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td class=\"center\">
\t\t\t\t\t\t\t\t\t\t\t{% if enseigne.id_enseigne not in joined %}
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#modalCenter-{{ enseigne.id_enseigne }}\" class=\"modal-basic mb-xs mt-xs mr-xs btn btn-success\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-reply \"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tRejoindre
\t\t\t\t\t\t\t\t\t\t\t\t</a>

\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"modalCenter-{{ enseigne.id_enseigne }}\" class=\"modal-block modal-block-primary mfp-hide\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Attention</h2>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body text-center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-wrapper\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-icon center\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-question-circle\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"modal-text\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4>Attention</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<p>Êtes-vous sur de vouloir rejoindre cette enseigne ?</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<footer class=\"panel-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12 text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('rejoindre', {'id_enseigne': enseigne.id_enseigne}) }}\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-primary\">Oui</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default modal-dismiss\">Non</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</footer>
\t\t\t\t\t\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t\t\t{% set coadmin = false %}
\t\t\t\t\t\t\t\t\t\t\t\t{% for enseigneJoind in enseignesJoined %}
\t\t\t\t\t\t\t\t\t\t\t\t\t{% if enseigneJoind.idUser == app.user.id and 'co_administrateur' in enseigneJoind.droits and enseigneJoind.idEnseigne == enseigne.id_enseigne %}
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"{{ path('membre', {'id_enseigne': enseigne.id_enseigne}) }}\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-users\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tMembres
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t{% set coadmin = true %}
\t\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t\t\t{% if coadmin == false %}
\t\t\t\t\t\t\t\t\t\t\t\t\tAucune(s)
\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t\t\t{#<a href=\"\" class=\"mb-xs mt-xs mr-xs btn btn-info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"fa fa-info\"></span>
\t\t\t\t\t\t\t\t\t\t\t\t\tMes détails
\t\t\t\t\t\t\t\t\t\t\t\t</a>#}
\t\t\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t</section>
\t\t\t{% endif %}
\t\t</div>
\t</div>
{% endblock %}

{% block javascripts %}
\t
\t<script src=\"{{ asset('assets/vendor/select2/select2.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }} \"></script>
\t<script src=\"{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}\"></script>
\t<script src=\"{{ asset('assets/javascripts/ui-elements/examples.modals.js') }}\"></script>
\t<script src=\"{{ asset('assets/javascripts/tables/examples.datatables.default.js') }}\"></script>
{% endblock %}
", "enseigne/index.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/enseigne/index.html.twig");
    }
}
