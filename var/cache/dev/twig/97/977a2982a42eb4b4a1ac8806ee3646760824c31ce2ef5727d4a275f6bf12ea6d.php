<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* services/index.html.twig */
class __TwigTemplate_713eece8d878bb2369d39ffa1976ce806037f20f9fb71a4fa1292d4958563d92 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'position' => [$this, 'block_position'],
            'chemin' => [$this, 'block_chemin'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "services/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "services/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "KhaMELIA - Service";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 4
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 5
        echo "    <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/css/datatables.css"), "html", null, true);
        echo " \" />
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.css"), "html", null, true);
        echo " \" />
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_position($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "position"));

        // line 11
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 11, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_chemin($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "chemin"));

        // line 15
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["chemin"]) || array_key_exists("chemin", $context) ? $context["chemin"] : (function () { throw new RuntimeError('Variable "chemin" does not exist.', 15, $this->source); })()), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 18
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 19
        echo "<div class=\"row\">
    <div class=\"col-md-6 col-lg-12 col-xl-6\">
        <section class=\"panel\">
            <header class=\"panel-heading\">
                <div class=\"panel-actions\">
                    ";
        // line 24
        if (twig_in_filter("SUPER_ADMIN", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 24, $this->source); })()), "user", [], "any", false, false, false, 24), "roles", [], "any", false, false, false, 24))) {
            // line 25
            echo "                        <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("services_new");
            echo "\" class=\"\">
                            <button class=\"mb-xs mt-xs mr-xs btn btn-success\">
                                <span class=\"fa fa-plus\"></span>
                                Créer
                            </button>
                            
                        </a>
                    ";
        }
        // line 33
        echo "                    <a href=\"#\" class=\"fa fa-caret-down\"></a>
                    
                </div>
        
                <h2 class=\"panel-title\">
                   Liste des services
                </h2>
                
            </header>
            <div class=\"panel-body\">
                <table class=\"table table-bordered table-striped mb-none\" ";
        // line 43
        if ( !twig_test_empty((isset($context["services"]) || array_key_exists("services", $context) ? $context["services"] : (function () { throw new RuntimeError('Variable "services" does not exist.', 43, $this->source); })()))) {
            echo "id=\"datatable-default\"";
        }
        echo " >
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Description</th>
                            <th>Etat</th>
                            <th>Type</th>
                            <th>actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        ";
        // line 54
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["services"]) || array_key_exists("services", $context) ? $context["services"] : (function () { throw new RuntimeError('Variable "services" does not exist.', 54, $this->source); })()));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["service"]) {
            // line 55
            echo "                            <tr>
                                <td>";
            // line 56
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "libelle", [], "any", false, false, false, 56), "html", null, true);
            echo "</td>
                                <td>";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "description", [], "any", false, false, false, 57), "html", null, true);
            echo "</td>
                                <td>";
            // line 58
            echo ((twig_get_attribute($this->env, $this->source, $context["service"], "etat", [], "any", false, false, false, 58)) ? ("Activé") : ("Désactivé"));
            echo "</td>
                                <td>";
            // line 59
            echo ((twig_get_attribute($this->env, $this->source, $context["service"], "type", [], "any", false, false, false, 59)) ? ("Obligatoire") : ("Pas obligatoire"));
            echo "</td>
                                <td>
                                    ";
            // line 61
            if (twig_in_filter("SUPER_ADMIN", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 61, $this->source); })()), "user", [], "any", false, false, false, 61), "roles", [], "any", false, false, false, 61))) {
                // line 62
                echo "                                        <a href=\"#modalCenter-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 62), "html", null, true);
                echo " \" class=\"mb-xs mt-xs mr-xs btn btn-default modal-basic\">
                                            ";
                // line 63
                echo ((twig_get_attribute($this->env, $this->source, $context["service"], "etat", [], "any", false, false, false, 63)) ? ("Désactivé") : ("Activé"));
                echo "
                                        </a>
                                        <div id=\"modalCenter-";
                // line 65
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 65), "html", null, true);
                echo "\" class=\"modal-block modal-block-primary mfp-hide\">
                                            <section class=\"panel\">
                                                <header class=\"panel-heading\">
                                                    <h2 class=\"panel-title\">Attention</h2>
                                                </header>
                                                <div class=\"panel-body text-center\">
                                                    <div class=\"modal-wrapper\">
                                                        <div class=\"modal-icon center\">
                                                            <i class=\"fa fa-question-circle\"></i>
                                                        </div>
                                                        <div class=\"modal-text\">
                                                            <h4>Attention</h4>
                                                            <p>Êtes-vous sur de vouloir changer l'état de cet service ?</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <footer class=\"panel-footer\">
                                                    <div class=\"row\">
                                                        <div class=\"col-md-12 text-right\">
                                                            <a href=\"";
                // line 84
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("services_active", ["id" => twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 84)]), "html", null, true);
                echo "\">
                                                                <button class=\"btn btn-primary\">Oui</button>
                                                            </a>
                                                            <button class=\"btn btn-default modal-dismiss\">Non</button>
                                                        </div>
                                                    </div>
                                                </footer>
                                            </section>
                                        </div>
                                        <a href=\"";
                // line 93
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("services_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 93)]), "html", null, true);
                echo "\" class=\"mb-xs mt-xs mr-xs btn btn-primary\"><span class=\"fa fa-edit\"></span> Modifier</a>
                                    ";
            } else {
                // line 95
                echo "                                        ";
                if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["service"], "type", [], "any", false, false, false, 95), true))) {
                    // line 96
                    echo "                                                Déjà Membre
                                        ";
                } else {
                    // line 98
                    echo "                                            ";
                    if ((isset($context["joinedEnseigne"]) || array_key_exists("joinedEnseigne", $context) ? $context["joinedEnseigne"] : (function () { throw new RuntimeError('Variable "joinedEnseigne" does not exist.', 98, $this->source); })())) {
                        // line 99
                        echo "                                                ";
                        $context["dejaMembre"] = false;
                        // line 100
                        echo "                                                ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 100, $this->source); })()), "user", [], "any", false, false, false, 100), "services", [], "any", false, false, false, 100));
                        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                            // line 101
                            echo "                                                    ";
                            if ((0 === twig_compare(twig_get_attribute($this->env, $this->source, $context["item"], "id", [], "any", false, false, false, 101), twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 101)))) {
                                // line 102
                                echo "                                                        Déjà membre
                                                        ";
                                // line 103
                                $context["dejaMembre"] = true;
                                // line 104
                                echo "                                                    ";
                            }
                            // line 105
                            echo "                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 106
                        echo "                                                ";
                        if ((0 === twig_compare((isset($context["dejaMembre"]) || array_key_exists("dejaMembre", $context) ? $context["dejaMembre"] : (function () { throw new RuntimeError('Variable "dejaMembre" does not exist.', 106, $this->source); })()), false))) {
                            // line 107
                            echo "                                                    <a href=\"#modalCenterJoin-";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 107), "html", null, true);
                            echo "\" class=\"mb-xs mt-xs mr-xs btn btn-primary modal-basic\"><span class=\"fa fa-reply\"></span> Rejoindre</a>

                                                    <div id=\"modalCenterJoin-";
                            // line 109
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 109), "html", null, true);
                            echo "\" class=\"modal-block modal-block-primary mfp-hide\">
                                                        <section class=\"panel\">
                                                            <header class=\"panel-heading\">
                                                                <h2 class=\"panel-title\">Attention</h2>
                                                            </header>
                                                            <div class=\"panel-body text-center\">
                                                                <div class=\"modal-wrapper\">
                                                                    <div class=\"modal-icon center\">
                                                                        <i class=\"fa fa-question-circle\"></i>
                                                                    </div>
                                                                    <div class=\"modal-text\">
                                                                        <h4>Attention</h4>
                                                                        <p>Êtes-vous sur de vouloir rejoindre ce service ?</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <footer class=\"panel-footer\">
                                                                <div class=\"row\">
                                                                    <div class=\"col-md-12 text-right\">
                                                                        <a href=\"";
                            // line 128
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("service_join", ["id" => twig_get_attribute($this->env, $this->source, $context["service"], "id", [], "any", false, false, false, 128)]), "html", null, true);
                            echo "\">
                                                                            <button class=\"btn btn-primary\">Oui</button>
                                                                        </a>
                                                                        <button class=\"btn btn-default modal-dismiss\">Non</button>
                                                                    </div>
                                                                </div>
                                                            </footer>
                                                        </section>
                                                    </div>
                                                ";
                        }
                        // line 138
                        echo "                                            ";
                    } else {
                        // line 139
                        echo "                                                Rejoignez d'abord une enseigne
                                            ";
                    }
                    // line 141
                    echo "
                                        ";
                }
                // line 143
                echo "                                    ";
            }
            // line 144
            echo "                                </td>
                            </tr>
                        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 147
            echo "                            <tr>
                                <td colspan=\"5\">Aucun service créé</td>
                            </tr>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['service'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 151
        echo "                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>

    
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 161
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 162
        echo "    
    <script src=\"";
        // line 163
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/select2/select2.js"), "html", null, true);
        echo " \"></script>
    <script src=\"";
        // line 164
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"), "html", null, true);
        echo " \"></script>
    <script src=\"";
        // line 165
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"), "html", null, true);
        echo " \"></script>
    <script src=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"), "html", null, true);
        echo " \"></script>
    <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/pnotify/pnotify.custom.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/ui-elements/examples.modals.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 169
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/tables/examples.datatables.default.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "services/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  458 => 169,  454 => 168,  450 => 167,  446 => 166,  442 => 165,  438 => 164,  434 => 163,  431 => 162,  421 => 161,  403 => 151,  394 => 147,  387 => 144,  384 => 143,  380 => 141,  376 => 139,  373 => 138,  360 => 128,  338 => 109,  332 => 107,  329 => 106,  323 => 105,  320 => 104,  318 => 103,  315 => 102,  312 => 101,  307 => 100,  304 => 99,  301 => 98,  297 => 96,  294 => 95,  289 => 93,  277 => 84,  255 => 65,  250 => 63,  245 => 62,  243 => 61,  238 => 59,  234 => 58,  230 => 57,  226 => 56,  223 => 55,  218 => 54,  202 => 43,  190 => 33,  178 => 25,  176 => 24,  169 => 19,  159 => 18,  146 => 15,  136 => 14,  123 => 11,  113 => 10,  101 => 7,  97 => 6,  92 => 5,  82 => 4,  63 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}KhaMELIA - Service{% endblock %}
{% block stylesheets %}
    <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/select2/select2.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css') }} \" />
    <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/pnotify/pnotify.custom.css') }} \" />
{% endblock %}

{% block position %}
    {{ position }}
{% endblock %}

{% block chemin %}
    {{ chemin }}
{% endblock %}

{% block body %}
<div class=\"row\">
    <div class=\"col-md-6 col-lg-12 col-xl-6\">
        <section class=\"panel\">
            <header class=\"panel-heading\">
                <div class=\"panel-actions\">
                    {% if 'SUPER_ADMIN' in app.user.roles %}
                        <a href=\"{{ path('services_new') }}\" class=\"\">
                            <button class=\"mb-xs mt-xs mr-xs btn btn-success\">
                                <span class=\"fa fa-plus\"></span>
                                Créer
                            </button>
                            
                        </a>
                    {% endif %}
                    <a href=\"#\" class=\"fa fa-caret-down\"></a>
                    
                </div>
        
                <h2 class=\"panel-title\">
                   Liste des services
                </h2>
                
            </header>
            <div class=\"panel-body\">
                <table class=\"table table-bordered table-striped mb-none\" {% if services is not empty %}id=\"datatable-default\"{% endif %} >
                    <thead>
                        <tr>
                            <th>Libelle</th>
                            <th>Description</th>
                            <th>Etat</th>
                            <th>Type</th>
                            <th>actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for service in services %}
                            <tr>
                                <td>{{ service.libelle }}</td>
                                <td>{{ service.description }}</td>
                                <td>{{ service.etat ? 'Activé' : 'Désactivé' }}</td>
                                <td>{{ service.type ? 'Obligatoire' : 'Pas obligatoire' }}</td>
                                <td>
                                    {% if 'SUPER_ADMIN' in app.user.roles %}
                                        <a href=\"#modalCenter-{{ service.id }} \" class=\"mb-xs mt-xs mr-xs btn btn-default modal-basic\">
                                            {{ service.etat ? 'Désactivé' : 'Activé' }}
                                        </a>
                                        <div id=\"modalCenter-{{ service.id }}\" class=\"modal-block modal-block-primary mfp-hide\">
                                            <section class=\"panel\">
                                                <header class=\"panel-heading\">
                                                    <h2 class=\"panel-title\">Attention</h2>
                                                </header>
                                                <div class=\"panel-body text-center\">
                                                    <div class=\"modal-wrapper\">
                                                        <div class=\"modal-icon center\">
                                                            <i class=\"fa fa-question-circle\"></i>
                                                        </div>
                                                        <div class=\"modal-text\">
                                                            <h4>Attention</h4>
                                                            <p>Êtes-vous sur de vouloir changer l'état de cet service ?</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <footer class=\"panel-footer\">
                                                    <div class=\"row\">
                                                        <div class=\"col-md-12 text-right\">
                                                            <a href=\"{{ path('services_active', {'id': service.id}) }}\">
                                                                <button class=\"btn btn-primary\">Oui</button>
                                                            </a>
                                                            <button class=\"btn btn-default modal-dismiss\">Non</button>
                                                        </div>
                                                    </div>
                                                </footer>
                                            </section>
                                        </div>
                                        <a href=\"{{ path('services_edit', {'id': service.id}) }}\" class=\"mb-xs mt-xs mr-xs btn btn-primary\"><span class=\"fa fa-edit\"></span> Modifier</a>
                                    {% else %}
                                        {% if service.type == true %}
                                                Déjà Membre
                                        {% else %}
                                            {% if joinedEnseigne %}
                                                {% set dejaMembre = false %}
                                                {% for item in app.user.services %}
                                                    {% if item.id == service.id %}
                                                        Déjà membre
                                                        {% set dejaMembre = true %}
                                                    {% endif %}
                                                {% endfor %}
                                                {% if dejaMembre == false %}
                                                    <a href=\"#modalCenterJoin-{{ service.id }}\" class=\"mb-xs mt-xs mr-xs btn btn-primary modal-basic\"><span class=\"fa fa-reply\"></span> Rejoindre</a>

                                                    <div id=\"modalCenterJoin-{{ service.id }}\" class=\"modal-block modal-block-primary mfp-hide\">
                                                        <section class=\"panel\">
                                                            <header class=\"panel-heading\">
                                                                <h2 class=\"panel-title\">Attention</h2>
                                                            </header>
                                                            <div class=\"panel-body text-center\">
                                                                <div class=\"modal-wrapper\">
                                                                    <div class=\"modal-icon center\">
                                                                        <i class=\"fa fa-question-circle\"></i>
                                                                    </div>
                                                                    <div class=\"modal-text\">
                                                                        <h4>Attention</h4>
                                                                        <p>Êtes-vous sur de vouloir rejoindre ce service ?</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <footer class=\"panel-footer\">
                                                                <div class=\"row\">
                                                                    <div class=\"col-md-12 text-right\">
                                                                        <a href=\"{{ path('service_join', {'id': service.id}) }}\">
                                                                            <button class=\"btn btn-primary\">Oui</button>
                                                                        </a>
                                                                        <button class=\"btn btn-default modal-dismiss\">Non</button>
                                                                    </div>
                                                                </div>
                                                            </footer>
                                                        </section>
                                                    </div>
                                                {% endif %}
                                            {% else %}
                                                Rejoignez d'abord une enseigne
                                            {% endif %}

                                        {% endif %}
                                    {% endif %}
                                </td>
                            </tr>
                        {% else %}
                            <tr>
                                <td colspan=\"5\">Aucun service créé</td>
                            </tr>
                        {% endfor %}
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>

    
{% endblock %}

{% block javascripts %}
    
    <script src=\"{{ asset('assets/vendor/select2/select2.js') }} \"></script>
    <script src=\"{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }} \"></script>
    <script src=\"{{ asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }} \"></script>
    <script src=\"{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }} \"></script>
    <script src=\"{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}\"></script>
    <script src=\"{{ asset('assets/javascripts/ui-elements/examples.modals.js') }}\"></script>
    <script src=\"{{ asset('assets/javascripts/tables/examples.datatables.default.js') }}\"></script>
{% endblock %}
", "services/index.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/services/index.html.twig");
    }
}
