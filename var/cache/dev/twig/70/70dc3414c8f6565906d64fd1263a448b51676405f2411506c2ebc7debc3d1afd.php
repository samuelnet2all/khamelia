<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_bdf994d5b6aefba289b5990888b066954e5f060530407184a94b0a7e712eae1b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta name=\"keywords\" content=\"InetSCHOOL\" />
        <meta name=\"description\" content=\"InetSCHOOL pages\">

        <!-- Mobile Metas -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />

        <!-- Web Fonts  -->
        <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light\" rel=\"stylesheet\" type=\"text/css\">

        <!-- Vendor CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/magnific-popup.css"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/css/datepicker3.css"), "html", null, true);
        echo "\" />
        
        <!-- Specific Page Vendor CSS -->
        ";
        // line 22
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 23
        echo "        <!-- Theme CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/theme.css"), "html", null, true);
        echo "\" />

        <!-- Skin CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/skins/default.css"), "html", null, true);
        echo "\" />

        <!-- Theme Custom CSS -->
        <link rel=\"stylesheet\" href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/theme-custom.css"), "html", null, true);
        echo "\">

        <!-- Head Libs -->
        <script src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>
        
    </head>
    <body>
    <section class=\"body-sign\">
        <div class=\"center-sign\">
            <a href=\"/\" class=\"logo pull-left\">
                <img src=\"assets/images/logo.png\" height=\"54\" alt=\"Porto Admin\" />
            </a>

            <div class=\"panel panel-sign\">
                <div class=\"panel-title-sign mt-xl text-right\">
                    <h2 class=\"title text-uppercase text-bold m-none\"><i class=\"fa fa-user mr-xs\"></i> Connexion</h2>
                </div>
                <div class=\"panel-body\">
                    <form method=\"post\">
                        ";
        // line 49
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 49, $this->source); })())) {
            // line 50
            echo "                            <div class=\"alert alert-danger\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
                                <strong>Problème rencontré!</strong> ";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 52, $this->source); })()), "messageKey", [], "any", false, false, false, 52), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 52, $this->source); })()), "messageData", [], "any", false, false, false, 52), "security"), "html", null, true);
            echo "
                            </div>
                        ";
        }
        // line 55
        echo "                        ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 55, $this->source); })()), "user", [], "any", false, false, false, 55)) {
            // line 56
            echo "                            <div class=\"alert alert-primary\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
                                <strong>Note !</strong> Vous êtes connectés en tant que ";
            // line 58
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 58, $this->source); })()), "user", [], "any", false, false, false, 58), "username", [], "any", false, false, false, 58), "html", null, true);
            echo "!  <a class=\"alert-link\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Logout</a>.
                            </div>
                        ";
        }
        // line 61
        echo "                        <div class=\"form-group mb-lg\">
                            <label for=\"inputEmail\">Email</label>
                            <div class=\"input-group input-group-icon\">
                            <input type=\"email\" value=\"";
        // line 64
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 64, $this->source); })()), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\" class=\"form-control input-lg\" required autofocus>
                                <span class=\"input-group-addon\">
                                    <span class=\"icon icon-lg\">
                                        <i class=\"fa fa-envelope\"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class=\"form-group mb-lg\">
                            <div class=\"clearfix\">
                                <label class=\"pull-left\" id=\"inputPassword\">Mot de passe</label>
                                <a href=\"pages-recover-password.html\" class=\"pull-right\">Mot de passe oublié?</a>
                            </div>
                            <div class=\"input-group input-group-icon\">
                                <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control input-lg\" required>
                                <span class=\"input-group-addon\">
                                    <span class=\"icon icon-lg\">
                                        <i class=\"fa fa-lock\"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-sm-8\">
                                <div class=\"checkbox-custom checkbox-default\">
                                    <input id=\"RememberMe\" name=\"rememberme\" type=\"checkbox\"/>
                                    <label for=\"RememberMe\">Se souvenir de moi</label>
                                </div>
                            </div>
                            <div class=\"col-sm-4 text-right\">
                                <button type=\"submit\" class=\"btn btn-primary hidden-xs\">Connexion</button>
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-lg visible-xs mt-lg\">Connexion</button>
                            </div>
                        </div>

                       ";
        // line 111
        echo "                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\">
                    </form>
                </div>
            </div>
            <p class=\"text-center text-muted mt-md mb-md\">JobsREADY ©2020 All Rights Reserved.</p>
        </div>
    </section>

    ";
        // line 128
        echo "
  ";
        // line 138
        echo "
    ";
        // line 149
        echo "
    ";
        // line 153
        echo "
 <!-- Vendor -->
        <script src=\"";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 158
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/nanoscroller/nanoscroller.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 159
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 160
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/magnific-popup.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-placeholder/jquery.placeholder.js"), "html", null, true);
        echo "\"></script>
        
        <!-- Specific Page Vendor -->
         ";
        // line 164
        $this->displayBlock('javascripts', $context, $blocks);
        // line 165
        echo "        
        <!-- Theme Base, Components and Settings -->
        <script src=\"";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.js"), "html", null, true);
        echo "\"></script>
        
        <!-- Theme Custom -->
        <script src=\"";
        // line 170
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.custom.js"), "html", null, true);
        echo "\"></script>
        
        <!-- Theme Initialization Files -->
        <script src=\"";
        // line 173
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.init.js"), "html", null, true);
        echo "\"></script>


        <!-- Examples -->
        <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/dashboard/examples.dashboard.js"), "html", null, true);
        echo "\"></script>
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "KhaMELIA - Connexion";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 22
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 164
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 164,  311 => 22,  292 => 5,  278 => 177,  271 => 173,  265 => 170,  259 => 167,  255 => 165,  253 => 164,  247 => 161,  243 => 160,  239 => 159,  235 => 158,  231 => 157,  227 => 156,  223 => 155,  219 => 153,  216 => 149,  213 => 138,  210 => 128,  198 => 111,  158 => 64,  153 => 61,  145 => 58,  141 => 56,  138 => 55,  132 => 52,  128 => 50,  126 => 49,  107 => 33,  101 => 30,  95 => 27,  89 => 24,  86 => 23,  84 => 22,  78 => 19,  74 => 18,  70 => 17,  66 => 16,  52 => 5,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}KhaMELIA - Connexion{% endblock %}</title>
        <meta name=\"keywords\" content=\"InetSCHOOL\" />
        <meta name=\"description\" content=\"InetSCHOOL pages\">

        <!-- Mobile Metas -->
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />

        <!-- Web Fonts  -->
        <link href=\"http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light\" rel=\"stylesheet\" type=\"text/css\">

        <!-- Vendor CSS -->
        <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/font-awesome/css/font-awesome.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/magnific-popup/magnific-popup.css') }}\" />
        <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}\" />
        
        <!-- Specific Page Vendor CSS -->
        {% block stylesheets %}{% endblock %}
        <!-- Theme CSS -->
        <link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/theme.css') }}\" />

        <!-- Skin CSS -->
        <link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/skins/default.css') }}\" />

        <!-- Theme Custom CSS -->
        <link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/theme-custom.css') }}\">

        <!-- Head Libs -->
        <script src=\"{{ asset('assets/vendor/modernizr/modernizr.js') }}\"></script>
        
    </head>
    <body>
    <section class=\"body-sign\">
        <div class=\"center-sign\">
            <a href=\"/\" class=\"logo pull-left\">
                <img src=\"assets/images/logo.png\" height=\"54\" alt=\"Porto Admin\" />
            </a>

            <div class=\"panel panel-sign\">
                <div class=\"panel-title-sign mt-xl text-right\">
                    <h2 class=\"title text-uppercase text-bold m-none\"><i class=\"fa fa-user mr-xs\"></i> Connexion</h2>
                </div>
                <div class=\"panel-body\">
                    <form method=\"post\">
                        {% if error %}
                            <div class=\"alert alert-danger\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
                                <strong>Problème rencontré!</strong> {{ error.messageKey|trans(error.messageData, 'security') }}
                            </div>
                        {% endif %}
                        {% if app.user %}
                            <div class=\"alert alert-primary\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
                                <strong>Note !</strong> Vous êtes connectés en tant que {{ app.user.username }}!  <a class=\"alert-link\" href=\"{{ path('app_logout') }}\">Logout</a>.
                            </div>
                        {% endif %}
                        <div class=\"form-group mb-lg\">
                            <label for=\"inputEmail\">Email</label>
                            <div class=\"input-group input-group-icon\">
                            <input type=\"email\" value=\"{{ last_username }}\" name=\"email\" id=\"inputEmail\" class=\"form-control input-lg\" required autofocus>
                                <span class=\"input-group-addon\">
                                    <span class=\"icon icon-lg\">
                                        <i class=\"fa fa-envelope\"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class=\"form-group mb-lg\">
                            <div class=\"clearfix\">
                                <label class=\"pull-left\" id=\"inputPassword\">Mot de passe</label>
                                <a href=\"pages-recover-password.html\" class=\"pull-right\">Mot de passe oublié?</a>
                            </div>
                            <div class=\"input-group input-group-icon\">
                                <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control input-lg\" required>
                                <span class=\"input-group-addon\">
                                    <span class=\"icon icon-lg\">
                                        <i class=\"fa fa-lock\"></i>
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class=\"row\">
                            <div class=\"col-sm-8\">
                                <div class=\"checkbox-custom checkbox-default\">
                                    <input id=\"RememberMe\" name=\"rememberme\" type=\"checkbox\"/>
                                    <label for=\"RememberMe\">Se souvenir de moi</label>
                                </div>
                            </div>
                            <div class=\"col-sm-4 text-right\">
                                <button type=\"submit\" class=\"btn btn-primary hidden-xs\">Connexion</button>
                                <button type=\"submit\" class=\"btn btn-primary btn-block btn-lg visible-xs mt-lg\">Connexion</button>
                            </div>
                        </div>

                       {# <span class=\"mt-lg mb-lg line-thru text-center text-uppercase\">
                                                   <span>or</span>
                                               </span>
                       
                                               <div class=\"mb-xs text-center\">
                                                   <a class=\"btn btn-facebook mb-md ml-xs mr-xs\">Connect with <i class=\"fa fa-facebook\"></i></a>
                                                   <a class=\"btn btn-twitter mb-md ml-xs mr-xs\">Connect with <i class=\"fa fa-twitter\"></i></a>
                                               </div>
                       
                                               <p class=\"text-center\">Don't have an account yet? <a href=\"pages-signup.html\">Sign Up!</a>#}
                        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\">
                    </form>
                </div>
            </div>
            <p class=\"text-center text-muted mt-md mb-md\">JobsREADY ©2020 All Rights Reserved.</p>
        </div>
    </section>

    {# <form method=\"post\"> if error %}
        <div class=\"alert alert-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
    {% endif %}

    {% if app.user %}
        <div class=\"mb-3\">
            You are logged in as {{ app.user.username }}, <a href=\"{{ path('app_logout') }}\">Logout</a>
        </div>
    {% endif #}

  {#  <h1 class=\"h3 mb-3 font-weight-normal\">Please sign in</h1>
      <label for=\"inputEmail\">Email</label>
      <input type=\"email\" value=\"{{ last_username }}\" name=\"email\" id=\"inputEmail\" class=\"form-control\" required autofocus>
      <label for=\"inputPassword\">Password</label>
      <input type=\"password\" name=\"password\" id=\"inputPassword\" class=\"form-control\" required>
  
      <input type=\"hidden\" name=\"_csrf_token\"
             value=\"{{ csrf_token('authenticate') }}\"
      >#}

    {#
        Uncomment this section and add a remember_me option below your firewall to activate remember me functionality.
        See https://symfony.com/doc/current/security/remember_me.html

        <div class=\"checkbox mb-3\">
            <label>
                <input type=\"checkbox\" name=\"_remember_me\"> Remember me
            </label>
        </div>
    #}

    {#<button class=\"btn btn-lg btn-primary\" type=\"submit\">
            Sign in
        </button></form>#}

 <!-- Vendor -->
        <script src=\"{{ asset('assets/vendor/jquery/jquery.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/bootstrap/js/bootstrap.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/nanoscroller/nanoscroller.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/magnific-popup/magnific-popup.js') }}\"></script>
        <script src=\"{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.js') }}\"></script>
        
        <!-- Specific Page Vendor -->
         {% block javascripts %}{% endblock %}
        
        <!-- Theme Base, Components and Settings -->
        <script src=\"{{ asset('assets/javascripts/theme.js') }}\"></script>
        
        <!-- Theme Custom -->
        <script src=\"{{ asset('assets/javascripts/theme.custom.js') }}\"></script>
        
        <!-- Theme Initialization Files -->
        <script src=\"{{ asset('assets/javascripts/theme.init.js') }}\"></script>


        <!-- Examples -->
        <script src=\"{{ asset('assets/javascripts/dashboard/examples.dashboard.js') }}\"></script>
    </body>
</html>
", "security/login.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/security/login.html.twig");
    }
}
