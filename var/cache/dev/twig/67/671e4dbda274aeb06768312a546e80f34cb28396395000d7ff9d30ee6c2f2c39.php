<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* index.html.twig */
class __TwigTemplate_7e730474e2993cffdbfbebb08e65c5dd0e64eb7cf6096a9560ded7cf4f69ff91 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>InetSCHOOLS</title>
        <meta name=\"keywords\" content=\"InetSCHOOLS\" />
\t\t<meta name=\"description\" content=\"InetSCHOOLS pages\">

\t\t<!-- Mobile Metas -->
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />

\t\t<!-- Web Fonts  -->
\t\t<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light\" rel=\"stylesheet\" type=\"text/css\">

\t\t<!-- Vendor CSS -->
\t\t<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/css/bootstrap.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/css/font-awesome.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/magnific-popup.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/css/datepicker3.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css"), "html", null, true);
        echo "\" />
\t\t<link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/morris/morris.css"), "html", null, true);
        echo "\" />

\t\t<!-- Theme CSS -->
\t\t<link rel=\"stylesheet\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/theme.css"), "html", null, true);
        echo "\" />

\t\t<!-- Skin CSS -->
\t\t<link rel=\"stylesheet\" href=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/skins/default.css"), "html", null, true);
        echo "\" />

\t\t<!-- Theme Custom CSS -->
\t\t<link rel=\"stylesheet\" href=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/stylesheets/theme-custom.css"), "html", null, true);
        echo "\">

\t\t<!-- Head Libs -->
\t\t<script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/modernizr/modernizr.js"), "html", null, true);
        echo "\"></script>
        
    </head>
\t<section class=\"body\">
\t\t<!-- start: header -->
\t\t<header class=\"header\">
\t\t\t<div class=\"logo-container\">
\t\t\t\t<a href=\"../\" class=\"logo\">
\t\t\t\t\t<img src=\"assets/images/logo.png\" height=\"35\" alt=\"JSOFT Admin\" />
\t\t\t\t</a>
\t\t\t\t<div class=\"visible-xs toggle-sidebar-left\" data-toggle-class=\"sidebar-left-opened\" data-target=\"html\" data-fire-event=\"sidebar-left-opened\">
\t\t\t\t\t<i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t
\t\t\t<!-- start: search & user box -->
\t\t\t<div class=\"header-right\">
\t\t
\t\t\t\t<form action=\"pages-search-results.html\" class=\"search nav-form\">
\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
\t\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t
\t\t\t\t<span class=\"separator\"></span>
\t\t
\t\t\t\t<ul class=\"notifications\">
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-tasks\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">3</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu large\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">3</span>
\t\t\t\t\t\t\t\tTasks
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Generating Sales Report</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">60%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Importing Contacts</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">98%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"98\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 98%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Uploading something big</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">33%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"33\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 33%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-envelope\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">4</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">230</span>
\t\t\t\t\t\t\t\tMessages
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t
\t\t\t\t\t\t\t\t<hr />
\t\t
\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"view-more\">View All</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-bell\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">3</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">3</span>
\t\t\t\t\t\t\t\tAlerts
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-thumbs-down bg-danger\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Server is Down!</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Just now</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-lock bg-warning\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">User Locked</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">15 minutes ago</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-signal bg-success\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Connection Restaured</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">10/10/2014</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t
\t\t\t\t\t\t\t\t<hr />
\t\t
\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"view-more\">View All</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t
\t\t\t\t<span class=\"separator\"></span>
\t\t
\t\t\t\t<div id=\"userbox\" class=\"userbox\">
\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\">
\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\" data-lock-picture=\"assets/images/!logged-user.jpg\" />
\t\t\t\t\t\t</figure>
\t\t\t\t\t\t<div class=\"profile-info\" data-lock-name=\"John Doe\" data-lock-email=\"johndoe@JSOFT.com\">
\t\t\t\t\t\t\t<span class=\"name\">John Doe Junior</span>
\t\t\t\t\t\t\t<span class=\"role\">administrator</span>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t<i class=\"fa custom-caret\"></i>
\t\t\t\t\t</a>
\t\t
\t\t\t\t\t<div class=\"dropdown-menu\">
\t\t\t\t\t\t<ul class=\"list-unstyled\">
\t\t\t\t\t\t\t<li class=\"divider\"></li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"pages-user-profile.html\"><i class=\"fa fa-user\"></i> My Profile</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"#\" data-lock-screen=\"true\"><i class=\"fa fa-lock\"></i> Lock Screen</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"pages-signin.html\"><i class=\"fa fa-power-off\"></i> Logout</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- end: search & user box -->
\t\t</header>
\t\t<!-- end: header -->

\t\t<div class=\"inner-wrapper\">
\t\t\t<!-- start: sidebar -->
\t\t\t<aside id=\"sidebar-left\" class=\"sidebar-left\">
\t\t\t
\t\t\t\t<div class=\"sidebar-header\">
\t\t\t\t\t<div class=\"sidebar-title\">
\t\t\t\t\t\tNavigation
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"sidebar-toggle hidden-xs\" data-toggle-class=\"sidebar-left-collapsed\" data-target=\"html\" data-fire-event=\"sidebar-left-toggle\">
\t\t\t\t\t\t<i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t
\t\t\t\t<div class=\"nano\">
\t\t\t\t\t<div class=\"nano-content\">
\t\t\t\t\t\t<nav id=\"menu\" class=\"nav-main\" role=\"navigation\">
\t\t\t\t\t\t\t<ul class=\"nav nav-main\">
\t\t\t\t\t\t\t\t<li class=\"nav-active\">
\t\t\t\t\t\t\t\t\t<a href=\"index.html\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-home\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Dashboard</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"mailbox-folder.html\">
\t\t\t\t\t\t\t\t\t\t<span class=\"pull-right label label-primary\">182</span>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Mailbox</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-copy\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Pages</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-signup.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sign Up
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-signin.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sign In
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-recover-password.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Recover Password
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-lock-screen.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Locked Screen
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-user-profile.html\">
\t\t\t\t\t\t\t\t\t\t\t\t User Profile
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-session-timeout.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Session Timeout
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-calendar.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Calendar
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-timeline.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Timeline
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-media-gallery.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Media Gallery
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-invoice.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Invoice
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-blank.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Blank Page
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-404.html\">
\t\t\t\t\t\t\t\t\t\t\t\t 404
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-500.html\">
\t\t\t\t\t\t\t\t\t\t\t\t 500
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-log-viewer.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Log Viewer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-search-results.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Search Results
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-tasks\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>UI Elements</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-typography.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Typography
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-icons.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Icons
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-tabs.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Tabs
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-panels.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Panels
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-widgets.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Widgets
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-portlets.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Portlets
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-buttons.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Buttons
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-alerts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Alerts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-notifications.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Notifications
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-modals.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Modals
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-lightbox.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Lightbox
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-progressbars.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Progress Bars
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-sliders.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sliders
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-carousels.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Carousels
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-accordions.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Accordions
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-nestable.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Nestable
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-tree-view.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Tree View
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-grid-system.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Grid System
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-charts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Charts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-animations.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Animations
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-extra.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Extra
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Forms</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-basic.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-advanced.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Advanced
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-validation.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Validation
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-layouts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Layouts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-wizard.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Wizard
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-code-editor.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Code Editor
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-table\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Tables</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-basic.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-advanced.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Advanced
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-responsive.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Responsive
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-editable.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Editable
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-ajax.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Ajax
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-pricing.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Pricing
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Maps</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-google-maps.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-google-maps-builder.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Map Builder
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-vector.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Vector
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-columns\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Layouts</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-default.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Default
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-boxed.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Boxed
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-menu-collapsed.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Menu Collapsed
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-scroll.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Scroll
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-align-left\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Menu Levels</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a>First Level</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t\t\t<a>Second Level</a>
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level Link #1</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level Link #2</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Second Level Link #1</a>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Second Level Link #2</a>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"http://themeforest.net/item/JSOFT-responsive-html5-template/4106987?ref=JSOFT\" target=\"_blank\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-external-link\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Front-End <em class=\"not-included\">(Not Included)</em></span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</nav>
\t\t\t
\t\t\t\t\t\t<hr class=\"separator\" />
\t\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-tasks\">
\t\t\t\t\t\t\t<div class=\"widget-header\">
\t\t\t\t\t\t\t\t<h6>Projects</h6>
\t\t\t\t\t\t\t\t<div class=\"widget-toggle\">+</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"widget-content\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled m-none\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JSOFT HTML5 Template</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Tucson Template</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JSOFT Admin</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t<hr class=\"separator\" />
\t\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-stats\">
\t\t\t\t\t\t\t<div class=\"widget-header\">
\t\t\t\t\t\t\t\t<h6>Company Stats</h6>
\t\t\t\t\t\t\t\t<div class=\"widget-toggle\">+</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"widget-content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 1</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">85%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">85% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 2</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">70%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">70% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 3</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">2%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"2\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 2%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">2% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t</aside>
\t\t\t<!-- end: sidebar -->

\t\t\t<section role=\"main\" class=\"content-body\">
\t\t\t\t<header class=\"page-header\">
\t\t\t\t\t<h2>Dashboard</h2>
\t\t\t\t
\t\t\t\t\t<div class=\"right-wrapper pull-right\">
\t\t\t\t\t\t<ol class=\"breadcrumbs\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"index.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-home\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li><span>Dashboard</span></li>
\t\t\t\t\t\t</ol>
\t\t\t\t
\t\t\t\t\t\t<a class=\"sidebar-right-toggle\" data-open=\"sidebar-right\"><i class=\"fa fa-chevron-left\"></i></a>
\t\t\t\t\t</div>
\t\t\t\t</header>

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Best Seller</h2>
\t\t\t\t\t\t\t\t<p class=\"panel-subtitle\">Customize the graphs as much as you want, there are so many options and features to display information using JSOFT Admin Template.</p>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t<!-- Flot: Basic -->
\t\t\t\t\t\t\t\t<div class=\"chart chart-md\" id=\"flotDashBasic\"></div>
\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\tvar flotDashBasicData = [{
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 170],
\t\t\t\t\t\t\t\t\t\t\t[1, 169],
\t\t\t\t\t\t\t\t\t\t\t[2, 173],
\t\t\t\t\t\t\t\t\t\t\t[3, 188],
\t\t\t\t\t\t\t\t\t\t\t[4, 147],
\t\t\t\t\t\t\t\t\t\t\t[5, 113],
\t\t\t\t\t\t\t\t\t\t\t[6, 128],
\t\t\t\t\t\t\t\t\t\t\t[7, 169],
\t\t\t\t\t\t\t\t\t\t\t[8, 173],
\t\t\t\t\t\t\t\t\t\t\t[9, 128],
\t\t\t\t\t\t\t\t\t\t\t[10, 128]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 1\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#0088cc\"
\t\t\t\t\t\t\t\t\t}, {
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 115],
\t\t\t\t\t\t\t\t\t\t\t[1, 124],
\t\t\t\t\t\t\t\t\t\t\t[2, 114],
\t\t\t\t\t\t\t\t\t\t\t[3, 121],
\t\t\t\t\t\t\t\t\t\t\t[4, 115],
\t\t\t\t\t\t\t\t\t\t\t[5, 83],
\t\t\t\t\t\t\t\t\t\t\t[6, 102],
\t\t\t\t\t\t\t\t\t\t\t[7, 148],
\t\t\t\t\t\t\t\t\t\t\t[8, 147],
\t\t\t\t\t\t\t\t\t\t\t[9, 103],
\t\t\t\t\t\t\t\t\t\t\t[10, 113]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 2\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#2baab1\"
\t\t\t\t\t\t\t\t\t}, {
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 70],
\t\t\t\t\t\t\t\t\t\t\t[1, 69],
\t\t\t\t\t\t\t\t\t\t\t[2, 73],
\t\t\t\t\t\t\t\t\t\t\t[3, 88],
\t\t\t\t\t\t\t\t\t\t\t[4, 47],
\t\t\t\t\t\t\t\t\t\t\t[5, 13],
\t\t\t\t\t\t\t\t\t\t\t[6, 28],
\t\t\t\t\t\t\t\t\t\t\t[7, 69],
\t\t\t\t\t\t\t\t\t\t\t[8, 73],
\t\t\t\t\t\t\t\t\t\t\t[9, 28],
\t\t\t\t\t\t\t\t\t\t\t[10, 28]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 3\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#734ba9\"
\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Server Usage</h2>
\t\t\t\t\t\t\t\t<p class=\"panel-subtitle\">It's easy to create custom graphs on JSOFT Admin Template, there are several graph types that you can use, such as lines, bars, pie charts, etc...</p>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t<!-- Flot: Curves -->
\t\t\t\t\t\t\t\t<div class=\"chart chart-md\" id=\"flotDashRealTime\"></div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<!-- start: page -->
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8\">
\t\t\t\t\t\t\t\t\t\t<div class=\"chart-data-selector\" id=\"salesSelectorWrapper\">
\t\t\t\t\t\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t\t\t\t\t\tSales:
\t\t\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"salesSelector\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Admin\" selected>JSOFT Admin</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Drupal\" >JSOFT Drupal</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Wordpress\" >JSOFT Wordpress</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t\t</h2>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"salesSelectorItems\" class=\"chart-data-selector-items mt-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Admin -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Admin\" id=\"flotDashSales1\" class=\"chart-active\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales1Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 190],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 320],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 270],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 180]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#0088cc\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Drupal -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Drupal\" id=\"flotDashSales2\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales2Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 290],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 540],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 480],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 220],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 170],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 190]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#2baab1\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Wordpress -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Wordpress\" id=\"flotDashSales3\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales3Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 840],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 740],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 690],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 940],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 1180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 820],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 570],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 780]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#734ba9\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 text-center\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title mt-md\">Sales Goal</h2>
\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-wrapper liquid-meter-sm mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter\">
\t\t\t\t\t\t\t\t\t\t\t\t<meter min=\"0\" max=\"100\" value=\"35\" id=\"meterSales\"></meter>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-selector\" id=\"meterSalesSel\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"35\" class=\"active\">Monthly Goal</a>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"28\">Annual Goal</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-primary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-life-ring\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Support Questions</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">1281</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-primary\">(14 unread)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(view all)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-secondary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-secondary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-usd\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Total Profit</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">\$ 14,890.30</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(withdraw)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-tertiary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-tertiary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Orders</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">38</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(statement)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-quartenary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-quartenary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Visitors</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">3765</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(report)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Profile</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-group\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading bg-primary\">

\t\t\t\t\t\t\t\t\t\t<div class=\"widget-profile-info\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name text-semibold\">John Doe</h4>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"role\">Administrator</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">(edit profile)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div id=\"accordion\">
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion panel-accordion-first\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1One\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check\"></i> Tasks
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1One\" class=\"accordion-body collapse in\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"widget-todo-list\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"\" id=\"todoListItem1\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem1\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem2\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem2\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem3\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem3\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem4\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem4\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem5\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem5\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem6\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem6\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr class=\"solid mt-sm mb-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<form method=\"get\" class=\"form-horizontal form-bordered\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" tabindex=\"-1\">Add</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1Two\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"fa fa-comment\"></i> Messages
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1Two\" class=\"accordion-body collapse\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list mb-xlg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineBarDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">488</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Average Profile Visits</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar circular-bar-xs m-none mt-xs mr-md pull-right\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar-chart\" data-percent=\"45\" data-plugin-options='{ \"barColor\": \"#2baab1\", \"delay\": 300, \"size\": 50, \"lineWidth\": 4 }'>
\t\t\t\t\t\t\t\t\t\t\t\t<strong>Average</strong>
\t\t\t\t\t\t\t\t\t\t\t\t<label><span class=\"percent\">45</span>%</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">12</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Working Projects</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineLineDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">89</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Pending Tasks</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t<span class=\"label label-primary label-sm text-normal va-middle mr-sm\">198</span>
\t\t\t\t\t\t\t\t\t<span class=\"va-middle\">Friends</span>
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<hr class=\"dotted short\">
\t\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t<a class=\"text-uppercase text-muted\" href=\"#\">(View All)</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"panel-footer\">
\t\t\t\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-6 col-lg-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Company Activity</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"timeline timeline-simple mt-xlg mb-md\">
\t\t\t\t\t\t\t\t\t<div class=\"tm-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"tm-title\">
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"h5 text-uppercase\">November 2013</h3>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<ol class=\"tm-items\">
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tIt's awesome when we find a good solution for our projects, JSOFT Admin is <span class=\"text-primary\">#awesome</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tCheckout! How cool is that!
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"thumbnail-gallery\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"img-thumbnail lightbox\" href=\"assets/images/projects/project-4.jpg\" data-plugin-options='{ \"type\":\"image\" }'>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" width=\"215\" src=\"assets/images/projects/project-4.jpg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"zoom\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Global Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div id=\"vectorMapWorld\" style=\"height: 350px; width: 100%;\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Projects Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-striped mb-none\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Project</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Progress</th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>1</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>2</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>3</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t60%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>4</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive Business WordPress Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t90%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>5</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Admin HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 45%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t45%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>6</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-danger\">Danger</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t40%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>7</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 95%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t95%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- end: page -->
\t\t\t</section>
\t\t</div>

\t\t<aside id=\"sidebar-right\" class=\"sidebar-right\">
\t\t\t<div class=\"nano\">
\t\t\t\t<div class=\"nano-content\">
\t\t\t\t\t<a href=\"#\" class=\"mobile-close visible-xs\">
\t\t\t\t\t\tCollapse <i class=\"fa fa-chevron-right\"></i>
\t\t\t\t\t</a>
\t\t
\t\t\t\t\t<div class=\"sidebar-right-wrapper\">
\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-calendar\">
\t\t\t\t\t\t\t<h6>Upcoming Tasks</h6>
\t\t\t\t\t\t\t<div data-plugin-datepicker data-plugin-skin=\"dark\" ></div>
\t\t
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<time datetime=\"2014-04-19T00:00+00:00\">04/19/2014</time>
\t\t\t\t\t\t\t\t\t<span>Company Meeting</span>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-friends\">
\t\t\t\t\t\t\t<h6>Friends</h6>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"status-online\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-online\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-offline\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-offline\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</aside>
\t</section>
<script src=\"";
        // line 1560
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/jquery.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1561
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1562
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1563
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/nanoscroller/nanoscroller.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1564
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1565
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/magnific-popup/magnific-popup.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 1566
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-placeholder/jquery.placeholder.js"), "html", null, true);
        echo "\"></script>

\t<script src=\"";
        // line 1568
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1569
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1570
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-appear/jquery.appear.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1571
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1572
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-easypiechart/jquery.easypiechart.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1573
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1574
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot-tooltip/jquery.flot.tooltip.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1575
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.pie.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1576
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.categories.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1577
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/flot/jquery.flot.resize.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1578
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery-sparkline/jquery.sparkline.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1579
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/raphael/raphael.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1580
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/morris/morris.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1581
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/gauge/gauge.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1582
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/snap-svg/snap.svg.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1583
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/liquid-meter/liquid.meter.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1584
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/jquery.vmap.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1585
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1586
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/jquery.vmap.world.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1587
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1588
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1589
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1590
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1591
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"), "html", null, true);
        echo "\"></script>
\t<script src=\"";
        // line 1592
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"), "html", null, true);
        echo "\"></script>
<!-- Theme Base, Components and Settings -->
\t\t<script src=\"";
        // line 1594
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Theme Custom -->
\t\t<script src=\"";
        // line 1597
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.custom.js"), "html", null, true);
        echo "\"></script>
\t\t
\t\t<!-- Theme Initialization Files -->
\t\t<script src=\"";
        // line 1600
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/theme.init.js"), "html", null, true);
        echo "\"></script>


\t\t<!-- Examples -->
\t\t<script src=\"";
        // line 1604
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/javascripts/dashboard/examples.dashboard.js"), "html", null, true);
        echo "\"></script>
    </body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1786 => 1604,  1779 => 1600,  1773 => 1597,  1767 => 1594,  1762 => 1592,  1758 => 1591,  1754 => 1590,  1750 => 1589,  1746 => 1588,  1742 => 1587,  1738 => 1586,  1734 => 1585,  1730 => 1584,  1726 => 1583,  1722 => 1582,  1718 => 1581,  1714 => 1580,  1710 => 1579,  1706 => 1578,  1702 => 1577,  1698 => 1576,  1694 => 1575,  1690 => 1574,  1686 => 1573,  1682 => 1572,  1678 => 1571,  1674 => 1570,  1670 => 1569,  1666 => 1568,  1661 => 1566,  1657 => 1565,  1653 => 1564,  1649 => 1563,  1645 => 1562,  1641 => 1561,  1637 => 1560,  108 => 34,  102 => 31,  96 => 28,  90 => 25,  84 => 22,  80 => 21,  76 => 20,  72 => 19,  68 => 18,  64 => 17,  60 => 16,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>InetSCHOOLS</title>
        <meta name=\"keywords\" content=\"InetSCHOOLS\" />
\t\t<meta name=\"description\" content=\"InetSCHOOLS pages\">

\t\t<!-- Mobile Metas -->
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no\" />

\t\t<!-- Web Fonts  -->
\t\t<link href=\"http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light\" rel=\"stylesheet\" type=\"text/css\">

\t\t<!-- Vendor CSS -->
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap/css/bootstrap.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/font-awesome/css/font-awesome.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/magnific-popup/magnific-popup.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css') }}\" />
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/morris/morris.css') }}\" />

\t\t<!-- Theme CSS -->
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/theme.css') }}\" />

\t\t<!-- Skin CSS -->
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/skins/default.css') }}\" />

\t\t<!-- Theme Custom CSS -->
\t\t<link rel=\"stylesheet\" href=\"{{ asset('assets/stylesheets/theme-custom.css') }}\">

\t\t<!-- Head Libs -->
\t\t<script src=\"{{ asset('assets/vendor/modernizr/modernizr.js') }}\"></script>
        
    </head>
\t<section class=\"body\">
\t\t<!-- start: header -->
\t\t<header class=\"header\">
\t\t\t<div class=\"logo-container\">
\t\t\t\t<a href=\"../\" class=\"logo\">
\t\t\t\t\t<img src=\"assets/images/logo.png\" height=\"35\" alt=\"JSOFT Admin\" />
\t\t\t\t</a>
\t\t\t\t<div class=\"visible-xs toggle-sidebar-left\" data-toggle-class=\"sidebar-left-opened\" data-target=\"html\" data-fire-event=\"sidebar-left-opened\">
\t\t\t\t\t<i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
\t\t\t\t</div>
\t\t\t</div>
\t\t
\t\t\t<!-- start: search & user box -->
\t\t\t<div class=\"header-right\">
\t\t
\t\t\t\t<form action=\"pages-search-results.html\" class=\"search nav-form\">
\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i></button>
\t\t\t\t\t\t</span>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t
\t\t\t\t<span class=\"separator\"></span>
\t\t
\t\t\t\t<ul class=\"notifications\">
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-tasks\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">3</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu large\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">3</span>
\t\t\t\t\t\t\t\tTasks
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Generating Sales Report</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">60%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Importing Contacts</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">98%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"98\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 98%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<p class=\"clearfix mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-left\">Uploading something big</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message pull-right text-dark\">33%</span>
\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-xs light mb-xs\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"33\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 33%;\"></div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-envelope\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">4</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">230</span>
\t\t\t\t\t\t\t\tMessages
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Truncated message. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam, nec venenatis risus. Vestibulum blandit faucibus est et malesuada. Sed interdum cursus dui nec venenatis. Pellentesque non nisi lobortis, rutrum eros ut, convallis nisi. Sed tellus turpis, dignissim sit amet tristique quis, pretium id est. Sed aliquam diam diam, sit amet faucibus tellus ultricies eu. Aliquam lacinia nibh a metus bibendum, eu commodo eros commodo. Sed commodo molestie elit, a molestie lacus porttitor id. Donec facilisis varius sapien, ac fringilla velit porttitor et. Nam tincidunt gravida dui, sed pharetra odio pharetra nec. Duis consectetur venenatis pharetra. Vestibulum egestas nisi quis elementum elementum.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\" />
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lacinia orci. Proin vestibulum eget risus non luctus. Nunc cursus lacinia lacinia. Nulla molestie malesuada est ac tincidunt. Quisque eget convallis diam.</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t
\t\t\t\t\t\t\t\t<hr />
\t\t
\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"view-more\">View All</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-toggle notification-icon\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t<i class=\"fa fa-bell\"></i>
\t\t\t\t\t\t\t<span class=\"badge\">3</span>
\t\t\t\t\t\t</a>
\t\t
\t\t\t\t\t\t<div class=\"dropdown-menu notification-menu\">
\t\t\t\t\t\t\t<div class=\"notification-title\">
\t\t\t\t\t\t\t\t<span class=\"pull-right label label-default\">3</span>
\t\t\t\t\t\t\t\tAlerts
\t\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-thumbs-down bg-danger\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Server is Down!</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Just now</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-lock bg-warning\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">User Locked</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">15 minutes ago</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"clearfix\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-signal bg-success\"></i>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Connection Restaured</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">10/10/2014</span>
\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t
\t\t\t\t\t\t\t\t<hr />
\t\t
\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"view-more\">View All</a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t</ul>
\t\t
\t\t\t\t<span class=\"separator\"></span>
\t\t
\t\t\t\t<div id=\"userbox\" class=\"userbox\">
\t\t\t\t\t<a href=\"#\" data-toggle=\"dropdown\">
\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\" data-lock-picture=\"assets/images/!logged-user.jpg\" />
\t\t\t\t\t\t</figure>
\t\t\t\t\t\t<div class=\"profile-info\" data-lock-name=\"John Doe\" data-lock-email=\"johndoe@JSOFT.com\">
\t\t\t\t\t\t\t<span class=\"name\">John Doe Junior</span>
\t\t\t\t\t\t\t<span class=\"role\">administrator</span>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t<i class=\"fa custom-caret\"></i>
\t\t\t\t\t</a>
\t\t
\t\t\t\t\t<div class=\"dropdown-menu\">
\t\t\t\t\t\t<ul class=\"list-unstyled\">
\t\t\t\t\t\t\t<li class=\"divider\"></li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"pages-user-profile.html\"><i class=\"fa fa-user\"></i> My Profile</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"#\" data-lock-screen=\"true\"><i class=\"fa fa-lock\"></i> Lock Screen</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a role=\"menuitem\" tabindex=\"-1\" href=\"pages-signin.html\"><i class=\"fa fa-power-off\"></i> Logout</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- end: search & user box -->
\t\t</header>
\t\t<!-- end: header -->

\t\t<div class=\"inner-wrapper\">
\t\t\t<!-- start: sidebar -->
\t\t\t<aside id=\"sidebar-left\" class=\"sidebar-left\">
\t\t\t
\t\t\t\t<div class=\"sidebar-header\">
\t\t\t\t\t<div class=\"sidebar-title\">
\t\t\t\t\t\tNavigation
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"sidebar-toggle hidden-xs\" data-toggle-class=\"sidebar-left-collapsed\" data-target=\"html\" data-fire-event=\"sidebar-left-toggle\">
\t\t\t\t\t\t<i class=\"fa fa-bars\" aria-label=\"Toggle sidebar\"></i>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t
\t\t\t\t<div class=\"nano\">
\t\t\t\t\t<div class=\"nano-content\">
\t\t\t\t\t\t<nav id=\"menu\" class=\"nav-main\" role=\"navigation\">
\t\t\t\t\t\t\t<ul class=\"nav nav-main\">
\t\t\t\t\t\t\t\t<li class=\"nav-active\">
\t\t\t\t\t\t\t\t\t<a href=\"index.html\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-home\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Dashboard</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"mailbox-folder.html\">
\t\t\t\t\t\t\t\t\t\t<span class=\"pull-right label label-primary\">182</span>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-envelope\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Mailbox</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-copy\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Pages</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-signup.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sign Up
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-signin.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sign In
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-recover-password.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Recover Password
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-lock-screen.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Locked Screen
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-user-profile.html\">
\t\t\t\t\t\t\t\t\t\t\t\t User Profile
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-session-timeout.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Session Timeout
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-calendar.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Calendar
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-timeline.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Timeline
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-media-gallery.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Media Gallery
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-invoice.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Invoice
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-blank.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Blank Page
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-404.html\">
\t\t\t\t\t\t\t\t\t\t\t\t 404
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-500.html\">
\t\t\t\t\t\t\t\t\t\t\t\t 500
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-log-viewer.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Log Viewer
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"pages-search-results.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Search Results
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-tasks\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>UI Elements</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-typography.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Typography
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-icons.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Icons
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-tabs.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Tabs
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-panels.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Panels
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-widgets.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Widgets
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-portlets.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Portlets
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-buttons.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Buttons
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-alerts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Alerts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-notifications.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Notifications
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-modals.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Modals
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-lightbox.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Lightbox
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-progressbars.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Progress Bars
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-sliders.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Sliders
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-carousels.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Carousels
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-accordions.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Accordions
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-nestable.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Nestable
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-tree-view.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Tree View
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-grid-system.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Grid System
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-charts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Charts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-animations.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Animations
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"ui-elements-extra.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Extra
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-list-alt\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Forms</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-basic.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-advanced.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Advanced
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-validation.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Validation
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-layouts.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Layouts
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-wizard.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Wizard
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"forms-code-editor.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Code Editor
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-table\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Tables</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-basic.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-advanced.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Advanced
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-responsive.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Responsive
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-editable.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Editable
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-ajax.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Ajax
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"tables-pricing.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Pricing
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-map-marker\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Maps</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-google-maps.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Basic
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-google-maps-builder.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Map Builder
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"maps-vector.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Vector
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-columns\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Layouts</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-default.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Default
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-boxed.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Boxed
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-menu-collapsed.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Menu Collapsed
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a href=\"layouts-scroll.html\">
\t\t\t\t\t\t\t\t\t\t\t\t Scroll
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t<a>
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-align-left\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Menu Levels</span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<a>First Level</a>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t\t\t<a>Second Level</a>
\t\t\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t\t\t<li class=\"nav-parent\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"nav nav-children\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level Link #1</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Third Level Link #2</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Second Level Link #1</a>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t<a>Second Level Link #2</a>
\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<a href=\"http://themeforest.net/item/JSOFT-responsive-html5-template/4106987?ref=JSOFT\" target=\"_blank\">
\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-external-link\" aria-hidden=\"true\"></i>
\t\t\t\t\t\t\t\t\t\t<span>Front-End <em class=\"not-included\">(Not Included)</em></span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</nav>
\t\t\t
\t\t\t\t\t\t<hr class=\"separator\" />
\t\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-tasks\">
\t\t\t\t\t\t\t<div class=\"widget-header\">
\t\t\t\t\t\t\t\t<h6>Projects</h6>
\t\t\t\t\t\t\t\t<div class=\"widget-toggle\">+</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"widget-content\">
\t\t\t\t\t\t\t\t<ul class=\"list-unstyled m-none\">
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JSOFT HTML5 Template</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Tucson Template</a></li>
\t\t\t\t\t\t\t\t\t<li><a href=\"#\">JSOFT Admin</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t
\t\t\t\t\t\t<hr class=\"separator\" />
\t\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-stats\">
\t\t\t\t\t\t\t<div class=\"widget-header\">
\t\t\t\t\t\t\t\t<h6>Company Stats</h6>
\t\t\t\t\t\t\t\t<div class=\"widget-toggle\">+</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"widget-content\">
\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 1</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">85%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 85%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">85% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 2</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">70%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 70%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">70% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-title\">Stat 3</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"stats-complete\">2%</span>
\t\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary progress-without-number\" role=\"progressbar\" aria-valuenow=\"2\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 2%;\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"sr-only\">2% Complete</span>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t
\t\t\t\t</div>
\t\t\t
\t\t\t</aside>
\t\t\t<!-- end: sidebar -->

\t\t\t<section role=\"main\" class=\"content-body\">
\t\t\t\t<header class=\"page-header\">
\t\t\t\t\t<h2>Dashboard</h2>
\t\t\t\t
\t\t\t\t\t<div class=\"right-wrapper pull-right\">
\t\t\t\t\t\t<ol class=\"breadcrumbs\">
\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t<a href=\"index.html\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-home\"></i>
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t<li><span>Dashboard</span></li>
\t\t\t\t\t\t</ol>
\t\t\t\t
\t\t\t\t\t\t<a class=\"sidebar-right-toggle\" data-open=\"sidebar-right\"><i class=\"fa fa-chevron-left\"></i></a>
\t\t\t\t\t</div>
\t\t\t\t</header>

\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Best Seller</h2>
\t\t\t\t\t\t\t\t<p class=\"panel-subtitle\">Customize the graphs as much as you want, there are so many options and features to display information using JSOFT Admin Template.</p>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t<!-- Flot: Basic -->
\t\t\t\t\t\t\t\t<div class=\"chart chart-md\" id=\"flotDashBasic\"></div>
\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\tvar flotDashBasicData = [{
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 170],
\t\t\t\t\t\t\t\t\t\t\t[1, 169],
\t\t\t\t\t\t\t\t\t\t\t[2, 173],
\t\t\t\t\t\t\t\t\t\t\t[3, 188],
\t\t\t\t\t\t\t\t\t\t\t[4, 147],
\t\t\t\t\t\t\t\t\t\t\t[5, 113],
\t\t\t\t\t\t\t\t\t\t\t[6, 128],
\t\t\t\t\t\t\t\t\t\t\t[7, 169],
\t\t\t\t\t\t\t\t\t\t\t[8, 173],
\t\t\t\t\t\t\t\t\t\t\t[9, 128],
\t\t\t\t\t\t\t\t\t\t\t[10, 128]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 1\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#0088cc\"
\t\t\t\t\t\t\t\t\t}, {
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 115],
\t\t\t\t\t\t\t\t\t\t\t[1, 124],
\t\t\t\t\t\t\t\t\t\t\t[2, 114],
\t\t\t\t\t\t\t\t\t\t\t[3, 121],
\t\t\t\t\t\t\t\t\t\t\t[4, 115],
\t\t\t\t\t\t\t\t\t\t\t[5, 83],
\t\t\t\t\t\t\t\t\t\t\t[6, 102],
\t\t\t\t\t\t\t\t\t\t\t[7, 148],
\t\t\t\t\t\t\t\t\t\t\t[8, 147],
\t\t\t\t\t\t\t\t\t\t\t[9, 103],
\t\t\t\t\t\t\t\t\t\t\t[10, 113]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 2\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#2baab1\"
\t\t\t\t\t\t\t\t\t}, {
\t\t\t\t\t\t\t\t\t\tdata: [
\t\t\t\t\t\t\t\t\t\t\t[0, 70],
\t\t\t\t\t\t\t\t\t\t\t[1, 69],
\t\t\t\t\t\t\t\t\t\t\t[2, 73],
\t\t\t\t\t\t\t\t\t\t\t[3, 88],
\t\t\t\t\t\t\t\t\t\t\t[4, 47],
\t\t\t\t\t\t\t\t\t\t\t[5, 13],
\t\t\t\t\t\t\t\t\t\t\t[6, 28],
\t\t\t\t\t\t\t\t\t\t\t[7, 69],
\t\t\t\t\t\t\t\t\t\t\t[8, 73],
\t\t\t\t\t\t\t\t\t\t\t[9, 28],
\t\t\t\t\t\t\t\t\t\t\t[10, 28]
\t\t\t\t\t\t\t\t\t\t],
\t\t\t\t\t\t\t\t\t\tlabel: \"Series 3\",
\t\t\t\t\t\t\t\t\t\tcolor: \"#734ba9\"
\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Server Usage</h2>
\t\t\t\t\t\t\t\t<p class=\"panel-subtitle\">It's easy to create custom graphs on JSOFT Admin Template, there are several graph types that you can use, such as lines, bars, pie charts, etc...</p>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">

\t\t\t\t\t\t\t\t<!-- Flot: Curves -->
\t\t\t\t\t\t\t\t<div class=\"chart chart-md\" id=\"flotDashRealTime\"></div>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<!-- start: page -->
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-8\">
\t\t\t\t\t\t\t\t\t\t<div class=\"chart-data-selector\" id=\"salesSelectorWrapper\">
\t\t\t\t\t\t\t\t\t\t\t<h2>
\t\t\t\t\t\t\t\t\t\t\t\tSales:
\t\t\t\t\t\t\t\t\t\t\t\t<strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"salesSelector\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Admin\" selected>JSOFT Admin</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Drupal\" >JSOFT Drupal</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"JSOFT Wordpress\" >JSOFT Wordpress</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t\t\t\t\t</strong>
\t\t\t\t\t\t\t\t\t\t\t</h2>

\t\t\t\t\t\t\t\t\t\t\t<div id=\"salesSelectorItems\" class=\"chart-data-selector-items mt-sm\">
\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Admin -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Admin\" id=\"flotDashSales1\" class=\"chart-active\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales1Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 190],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 140],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 320],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 270],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 180]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#0088cc\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Drupal -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Drupal\" id=\"flotDashSales2\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales2Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 240],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 290],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 540],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 480],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 220],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 170],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 190]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#2baab1\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>

\t\t\t\t\t\t\t\t\t\t\t\t<!-- Flot: Sales JSOFT Wordpress -->
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"chart chart-sm\" data-sales-rel=\"JSOFT Wordpress\" id=\"flotDashSales3\" class=\"chart-hidden\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<script>

\t\t\t\t\t\t\t\t\t\t\t\t\tvar flotDashSales3Data = [{
\t\t\t\t\t\t\t\t\t\t\t\t\t    data: [
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jan\", 840],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Feb\", 740],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Mar\", 690],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Apr\", 940],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"May\", 1180],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jun\", 820],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Jul\", 570],
\t\t\t\t\t\t\t\t\t\t\t\t\t        [\"Aug\", 780]
\t\t\t\t\t\t\t\t\t\t\t\t\t    ],
\t\t\t\t\t\t\t\t\t\t\t\t\t    color: \"#734ba9\"
\t\t\t\t\t\t\t\t\t\t\t\t\t}];

\t\t\t\t\t\t\t\t\t\t\t\t\t// See: assets/javascripts/dashboard/examples.dashboard.js for more settings.

\t\t\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-lg-4 text-center\">
\t\t\t\t\t\t\t\t\t\t<h2 class=\"panel-title mt-md\">Sales Goal</h2>
\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-wrapper liquid-meter-sm mt-lg\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter\">
\t\t\t\t\t\t\t\t\t\t\t\t<meter min=\"0\" max=\"100\" value=\"35\" id=\"meterSales\"></meter>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"liquid-meter-selector\" id=\"meterSalesSel\">
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"35\" class=\"active\">Monthly Goal</a>
\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\" data-val=\"28\">Annual Goal</a>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6 col-lg-12 col-xl-6\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-primary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-primary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-life-ring\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Support Questions</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">1281</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"text-primary\">(14 unread)</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(view all)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-secondary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-secondary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-usd\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Total Profit</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">\$ 14,890.30</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(withdraw)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-tertiary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-tertiary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-shopping-cart\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Orders</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">38</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(statement)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-md-12 col-lg-6 col-xl-6\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-featured-left panel-featured-quartenary\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col widget-summary-col-icon\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-icon bg-quartenary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-user\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-summary-col\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"title\">Today's Visitors</h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"info\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<strong class=\"amount\">3765</strong>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"summary-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"text-muted text-uppercase\">(report)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t

\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Profile</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel panel-group\">
\t\t\t\t\t\t\t\t\t<header class=\"panel-heading bg-primary\">

\t\t\t\t\t\t\t\t\t\t<div class=\"widget-profile-info\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!logged-user.jpg\">
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name text-semibold\">John Doe</h4>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 class=\"role\">Administrator</h5>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"profile-footer\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"#\">(edit profile)</a>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t\t\t<div id=\"accordion\">
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion panel-accordion-first\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1One\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-check\"></i> Tasks
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1One\" class=\"accordion-body collapse in\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"widget-todo-list\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" checked=\"\" id=\"todoListItem1\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem1\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem2\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem2\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem3\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem3\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem4\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem4\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem5\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem5\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"checkbox-custom checkbox-default\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" id=\"todoListItem6\" class=\"todo-check\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<label class=\"todo-label\" for=\"todoListItem6\"><span>Lorem ipsum dolor sit amet</span></label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"todo-actions\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"todo-remove\" href=\"#\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-times\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<hr class=\"solid mt-sm mb-lg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<form method=\"get\" class=\"form-horizontal form-bordered\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group mb-md\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" tabindex=\"-1\">Add</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</form>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"panel panel-accordion\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-heading\">
\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"accordion-toggle\" data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse1Two\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t <i class=\"fa fa-comment\"></i> Messages
\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t</h4>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t<div id=\"collapse1Two\" class=\"accordion-body collapse\">
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list mb-xlg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"message\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-3 col-lg-6\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">My Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineBarDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineBarDashData = [5, 6, 7, 2, 0, 4 , 2, 4, 2, 0, 4 , 2, 4, 2, 0, 4];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">488</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Average Profile Visits</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar circular-bar-xs m-none mt-xs mr-md pull-right\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"circular-bar-chart\" data-percent=\"45\" data-plugin-options='{ \"barColor\": \"#2baab1\", \"delay\": 300, \"size\": 50, \"lineWidth\": 4 }'>
\t\t\t\t\t\t\t\t\t\t\t\t<strong>Average</strong>
\t\t\t\t\t\t\t\t\t\t\t\t<label><span class=\"percent\">45</span>%</label>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">12</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Working Projects</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"small-chart pull-right\" id=\"sparklineLineDash\"></div>
\t\t\t\t\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\t\t\t\t\tvar sparklineLineDashData = [15, 16, 17, 19, 10, 15, 13, 12, 12, 14, 16, 17];
\t\t\t\t\t\t\t\t\t\t</script>
\t\t\t\t\t\t\t\t\t\t<div class=\"h4 text-bold mb-none\">89</div>
\t\t\t\t\t\t\t\t\t\t<p class=\"text-xs text-muted mb-none\">Pending Tasks</p>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</section>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">
\t\t\t\t\t\t\t\t\t<span class=\"label label-primary label-sm text-normal va-middle mr-sm\">198</span>
\t\t\t\t\t\t\t\t\t<span class=\"va-middle\">Friends</span>
\t\t\t\t\t\t\t\t</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"content\">
\t\t\t\t\t\t\t\t\t<ul class=\"simple-user-list\">
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joseph Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t<figure class=\"image rounded\">
\t\t\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joe Junior\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Joe Junior</span>
\t\t\t\t\t\t\t\t\t\t\t<span class=\"message truncate\">Lorem ipsum dolor sit.</span>
\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t<hr class=\"dotted short\">
\t\t\t\t\t\t\t\t\t<div class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t<a class=\"text-uppercase text-muted\" href=\"#\">(View All)</a>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"panel-footer\">
\t\t\t\t\t\t\t\t<div class=\"input-group input-search\">
\t\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"q\" id=\"q\" placeholder=\"Search...\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t\t\t\t\t<button class=\"btn btn-default\" type=\"submit\"><i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xl-6 col-lg-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Company Activity</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"timeline timeline-simple mt-xlg mb-md\">
\t\t\t\t\t\t\t\t\t<div class=\"tm-body\">
\t\t\t\t\t\t\t\t\t\t<div class=\"tm-title\">
\t\t\t\t\t\t\t\t\t\t\t<h3 class=\"h5 text-uppercase\">November 2013</h3>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t<ol class=\"tm-items\">
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tIt's awesome when we find a good solution for our projects, JSOFT Admin is <span class=\"text-primary\">#awesome</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"tm-box\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"text-muted mb-none\">7 months ago.</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p>
\t\t\t\t\t\t\t\t\t\t\t\t\t\tCheckout! How cool is that!
\t\t\t\t\t\t\t\t\t\t\t\t\t</p>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"thumbnail-gallery\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"img-thumbnail lightbox\" href=\"assets/images/projects/project-4.jpg\" data-plugin-options='{ \"type\":\"image\" }'>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<img class=\"img-responsive\" width=\"215\" src=\"assets/images/projects/project-4.jpg\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"zoom\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t\t\t</ol>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel panel-transparent\">
\t\t\t\t\t\t\t<header class=\"panel-heading\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Global Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div id=\"vectorMapWorld\" style=\"height: 350px; width: 100%;\"></div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-6 col-md-12\">
\t\t\t\t\t\t<section class=\"panel\">
\t\t\t\t\t\t\t<header class=\"panel-heading panel-heading-transparent\">
\t\t\t\t\t\t\t\t<div class=\"panel-actions\">
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-caret-down\"></a>
\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"fa fa-times\"></a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<h2 class=\"panel-title\">Projects Stats</h2>
\t\t\t\t\t\t\t</header>
\t\t\t\t\t\t\t<div class=\"panel-body\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t\t\t\t\t<table class=\"table table-striped mb-none\">
\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<th>#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Project</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th>Progress</th>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>1</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>2</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 100%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t100%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>3</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 60%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t60%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>4</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>Tucson - Responsive Business WordPress Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 90%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t90%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>5</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Admin HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-warning\">Warning</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 45%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t45%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>6</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive HTML5 Template</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-danger\">Danger</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded m-none mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 40%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t40%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t<td>7</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>JSOFT - Responsive Drupal 7 Theme</td>
\t\t\t\t\t\t\t\t\t\t\t\t<td><span class=\"label label-success\">Success</span></td>
\t\t\t\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress progress-sm progress-half-rounded mt-xs light\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-primary\" role=\"progressbar\" aria-valuenow=\"60\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 95%;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t95%
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</section>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<!-- end: page -->
\t\t\t</section>
\t\t</div>

\t\t<aside id=\"sidebar-right\" class=\"sidebar-right\">
\t\t\t<div class=\"nano\">
\t\t\t\t<div class=\"nano-content\">
\t\t\t\t\t<a href=\"#\" class=\"mobile-close visible-xs\">
\t\t\t\t\t\tCollapse <i class=\"fa fa-chevron-right\"></i>
\t\t\t\t\t</a>
\t\t
\t\t\t\t\t<div class=\"sidebar-right-wrapper\">
\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-calendar\">
\t\t\t\t\t\t\t<h6>Upcoming Tasks</h6>
\t\t\t\t\t\t\t<div data-plugin-datepicker data-plugin-skin=\"dark\" ></div>
\t\t
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t\t\t<time datetime=\"2014-04-19T00:00+00:00\">04/19/2014</time>
\t\t\t\t\t\t\t\t\t<span>Company Meeting</span>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t\t<div class=\"sidebar-widget widget-friends\">
\t\t\t\t\t\t\t<h6>Friends</h6>
\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t<li class=\"status-online\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-online\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-offline\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t\t<li class=\"status-offline\">
\t\t\t\t\t\t\t\t\t<figure class=\"profile-picture\">
\t\t\t\t\t\t\t\t\t\t<img src=\"assets/images/!sample-user.jpg\" alt=\"Joseph Doe\" class=\"img-circle\">
\t\t\t\t\t\t\t\t\t</figure>
\t\t\t\t\t\t\t\t\t<div class=\"profile-info\">
\t\t\t\t\t\t\t\t\t\t<span class=\"name\">Joseph Doe Junior</span>
\t\t\t\t\t\t\t\t\t\t<span class=\"title\">Hey, how are you?</span>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</aside>
\t</section>
<script src=\"{{ asset('assets/vendor/jquery/jquery.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/bootstrap/js/bootstrap.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/nanoscroller/nanoscroller.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/magnific-popup/magnific-popup.js') }}\"></script>
<script src=\"{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.js') }}\"></script>

\t<script src=\"{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.pie.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.categories.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/flot/jquery.flot.resize.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jquery-sparkline/jquery.sparkline.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/raphael/raphael.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/morris/morris.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/gauge/gauge.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/snap-svg/snap.svg.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/liquid-meter/liquid.meter.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/jquery.vmap.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}\"></script>
\t<script src=\"{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}\"></script>
<!-- Theme Base, Components and Settings -->
\t\t<script src=\"{{ asset('assets/javascripts/theme.js') }}\"></script>
\t\t
\t\t<!-- Theme Custom -->
\t\t<script src=\"{{ asset('assets/javascripts/theme.custom.js') }}\"></script>
\t\t
\t\t<!-- Theme Initialization Files -->
\t\t<script src=\"{{ asset('assets/javascripts/theme.init.js') }}\"></script>


\t\t<!-- Examples -->
\t\t<script src=\"{{ asset('assets/javascripts/dashboard/examples.dashboard.js') }}\"></script>
    </body>
</html>

", "index.html.twig", "/opt/lampp/htdocs/KhaMELIA/templates/index.html.twig");
    }
}
