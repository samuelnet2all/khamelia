<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/dashboard/enseigne/create' => [[['_route' => 'create_enseigne', '_controller' => 'App\\Controller\\EnseigneController::create'], null, ['POST' => 0], null, false, false, null]],
        '/dashboard/enseigne' => [[['_route' => 'enseigne', '_controller' => 'App\\Controller\\EnseigneController::index'], null, ['GET' => 0], null, true, false, null]],
        '/dashboard/enseigne/profile' => [[['_route' => 'profile', '_controller' => 'App\\Controller\\EnseigneController::profiles'], null, ['POST' => 0], null, false, false, null]],
        '/dashboard/enseigne/droits' => [[['_route' => 'droits', '_controller' => 'App\\Controller\\EnseigneController::roles'], null, ['POST' => 0], null, false, false, null]],
        '/dashboard/enseigne/edit' => [[['_route' => 'edit_enseigne', '_controller' => 'App\\Controller\\EnseigneController::edit'], null, ['POST' => 0], null, true, false, null]],
        '/index' => [[['_route' => 'index', '_controller' => 'App\\Controller\\IndexController::index'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/dashboard' => [[['_route' => 'dashboard', '_controller' => 'App\\Controller\\IndexController::dashboard'], null, ['GET' => 0], null, false, false, null]],
        '/' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/services' => [[['_route' => 'services_index', '_controller' => 'App\\Controller\\ServicesController::index'], null, ['GET' => 0], null, true, false, null]],
        '/services/new' => [[['_route' => 'services_new', '_controller' => 'App\\Controller\\ServicesController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/specialite/enseigne' => [[['_route' => 'specialite_enseigne_index', '_controller' => 'App\\Controller\\SpecialiteEnseigneController::index'], null, ['GET' => 0], null, true, false, null]],
        '/specialite/enseigne/new' => [[['_route' => 'specialite_enseigne_new', '_controller' => 'App\\Controller\\SpecialiteEnseigneController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/type/ens' => [[['_route' => 'type_ens_index', '_controller' => 'App\\Controller\\TypeEnsController::index'], null, ['GET' => 0], null, true, false, null]],
        '/type/ens/new' => [[['_route' => 'type_ens_new', '_controller' => 'App\\Controller\\TypeEnsController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/dashboard/enseigne/(?'
                    .'|affilier/([^/]++)(*:209)'
                    .'|rejoindre/([^/]++)(*:235)'
                    .'|membres/([^/]++)(*:259)'
                .')'
                .'|/s(?'
                    .'|ervices/([^/]++)(?'
                        .'|(*:292)'
                        .'|/(?'
                            .'|edit(*:308)'
                            .'|joinService(*:327)'
                        .')'
                        .'|(*:336)'
                    .')'
                    .'|pecialite/enseigne/([^/]++)(?'
                        .'|(*:375)'
                        .'|/edit(*:388)'
                        .'|(*:396)'
                    .')'
                .')'
                .'|/type/ens/([^/]++)(?'
                    .'|(*:427)'
                    .'|/edit(*:440)'
                    .'|(*:448)'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        209 => [[['_route' => 'affilier', '_controller' => 'App\\Controller\\EnseigneController::affilier'], ['id_enseigne'], ['GET' => 0], null, false, true, null]],
        235 => [[['_route' => 'rejoindre', '_controller' => 'App\\Controller\\EnseigneController::rejoindre'], ['id_enseigne'], ['GET' => 0], null, false, true, null]],
        259 => [[['_route' => 'membre', '_controller' => 'App\\Controller\\EnseigneController::membres'], ['id_enseigne'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        292 => [[['_route' => 'services_active', '_controller' => 'App\\Controller\\ServicesController::active'], ['id'], ['GET' => 0], null, false, true, null]],
        308 => [[['_route' => 'services_edit', '_controller' => 'App\\Controller\\ServicesController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        327 => [[['_route' => 'service_join', '_controller' => 'App\\Controller\\ServicesController::join'], ['id'], ['GET' => 0], null, false, false, null]],
        336 => [[['_route' => 'services_delete', '_controller' => 'App\\Controller\\ServicesController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        375 => [[['_route' => 'specialite_enseigne_show', '_controller' => 'App\\Controller\\SpecialiteEnseigneController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        388 => [[['_route' => 'specialite_enseigne_edit', '_controller' => 'App\\Controller\\SpecialiteEnseigneController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        396 => [[['_route' => 'specialite_enseigne_delete', '_controller' => 'App\\Controller\\SpecialiteEnseigneController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        427 => [[['_route' => 'type_ens_show', '_controller' => 'App\\Controller\\TypeEnsController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        440 => [[['_route' => 'type_ens_edit', '_controller' => 'App\\Controller\\TypeEnsController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        448 => [
            [['_route' => 'type_ens_delete', '_controller' => 'App\\Controller\\TypeEnsController::delete'], ['id'], ['DELETE' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
