-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 30 oct. 2020 à 05:28
-- Version du serveur :  10.4.6-MariaDB
-- Version de PHP :  7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `khamelia_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20201023033131', '2020-10-27 21:24:33', 285),
('DoctrineMigrations\\Version20201023230552', '2020-10-27 21:24:33', 90),
('DoctrineMigrations\\Version20201024211710', '2020-10-27 21:24:34', 134),
('DoctrineMigrations\\Version20201026155908', '2020-10-27 21:24:34', 335),
('DoctrineMigrations\\Version20201028124257', '2020-10-28 13:43:12', 381),
('DoctrineMigrations\\Version20201030035800', '2020-10-30 04:58:20', 3682);

-- --------------------------------------------------------

--
-- Structure de la table `enseigne_type`
--

CREATE TABLE `enseigne_type` (
  `id_enseigne` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `enseigne_type`
--

INSERT INTO `enseigne_type` (`id_enseigne`, `type`) VALUES
(3, 'maternite'),
(4, 'centre_sante'),
(7, 'centre_vaccination');

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `etat` tinyint(1) NOT NULL,
  `type` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`id`, `libelle`, `description`, `logo`, `etat`, `type`) VALUES
(14, 'ff', 'ffffff', 'Capture d’écran_2020-10-04_16-42-46-5f9b95f8b89a1.png', 1, 1),
(15, 'test', 'test2', 'demande1-5f9b96289399c.png', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `nom`, `prenom`, `type`) VALUES
(45, 'Chakira@gmail.com', '[\"ENTREPRISE\"]', '$argon2id$v=19$m=65536,t=4,p=1$AVHrvdwgh9gxuyv2TlYl1A$qfMTXh28q0nt7r+xF956Fdz+KFiUBOl6I7+0stY4Wrc', 'FOFANA', 'Chakira', 6),
(49, 'francisagbannon@gmail.com', '[\"PARTICULIER\"]', '$argon2id$v=19$m=65536,t=4,p=1$DPRALUI7dQp8zSyMgghNXg$kFvzGKJq7POhhaihSl9lsefnAN1pEWaTcYoO1IsY56w', 'AGBANNON', 'Francis', 1),
(54, 'dcarmelo15@gmail.com', '[\"SUPER_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$xD+QTCsqqon4IJck5cJr5A$15GVQmoSKl+Mbg/izPUDWIWdLpOJAXx92/QB1i+TFcc', 'dzidzonu', 'kenneth', 1),
(62, 'ainamongannan@gmail.com', '[\"ENTREPRISE\"]', '$argon2id$v=19$m=65536,t=4,p=1$JIEkHYBtWbjN+X9/zR+EjA$3sGCiyKZxLiQ9pRT08Vjos+L/tQ1hTJm+0WUjyQNbl0', 'gannan', 'emile', 6);

-- --------------------------------------------------------

--
-- Structure de la table `user_joined_enseigne`
--

CREATE TABLE `user_joined_enseigne` (
  `id` int(11) NOT NULL,
  `id_enseigne` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `profiles` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)',
  `droits` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_joined_enseigne`
--

INSERT INTO `user_joined_enseigne` (`id`, `id_enseigne`, `id_user`, `profiles`, `droits`) VALUES
(1, 4, 49, 'a:2:{i:0;s:7:\"patient\";i:1;s:10:\"laborantin\";}', 'a:1:{i:0;s:17:\"co_administrateur\";}'),
(2, 4, 45, 'a:0:{}', 'a:0:{}');

-- --------------------------------------------------------

--
-- Structure de la table `user_services`
--

CREATE TABLE `user_services` (
  `user_id` int(11) NOT NULL,
  `services_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user_services`
--

INSERT INTO `user_services` (`user_id`, `services_id`) VALUES
(45, 14),
(45, 15),
(49, 14),
(49, 15);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `enseigne_type`
--
ALTER TABLE `enseigne_type`
  ADD PRIMARY KEY (`id_enseigne`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- Index pour la table `user_joined_enseigne`
--
ALTER TABLE `user_joined_enseigne`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_services`
--
ALTER TABLE `user_services`
  ADD PRIMARY KEY (`user_id`,`services_id`),
  ADD KEY `IDX_93BF0569A76ED395` (`user_id`),
  ADD KEY `IDX_93BF0569AEF5A6C1` (`services_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `user_joined_enseigne`
--
ALTER TABLE `user_joined_enseigne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `user_services`
--
ALTER TABLE `user_services`
  ADD CONSTRAINT `FK_93BF0569A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_93BF0569AEF5A6C1` FOREIGN KEY (`services_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
